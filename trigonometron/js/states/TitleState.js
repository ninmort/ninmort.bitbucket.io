var Formularium = Formularium || {};

var creditos;
var popup;
var tween = null;

Formularium.TitleState = function () {
    "use strict";
    Formularium.JSONLevelState.call(this);

    this.prefab_classes = {
        "text": Formularium.TextPrefab.prototype.constructor,
        "login_button": Formularium.LoginButton.prototype.constructor,
        "imagen" : Formularium.Imagen.prototype.constructor
    };
};

Formularium.TitleState.prototype = Object.create(Formularium.JSONLevelState.prototype);
Formularium.TitleState.prototype.constructor = Formularium.TitleState;

Formularium.TitleState.prototype.create = function () {
    "use strict";
    var formularium_data;
    Formularium.JSONLevelState.prototype.create.call(this);
    this.game.plugins.add(PhaserInput.Plugin);
    //var input = this.game.add.inputField(10, 90);
    var nombre = game.add.inputField(game.width / 2 - 85, game.height/2.3 +180 - 17, {
        font: '26px Arial',
        fill: '#0A1437',
        fillAlpha: 0.8,
        fontWeight: 'bold',
        forceCase: PhaserInput.ForceCase.upper,
        width: 150,
        max: 20,
        padding: 8,
        borderWidth: 1,
        borderColor: '#0A1437',
        borderRadius: 6,
        placeHolder: 'Username',
        textAlign: 'center',
        zoom: true
    });
    // nombre.onKeyboardClose.addOnce(function() {
    //     this.resizeBackgroundImage();
    // });
    //this.login_with_anonymous_button.novisible();
    this.game.usuario = nombre;

    creditos = game.add.button(60, game.world.height - 70, 'button', this.openWindow, this, 2, 1, 0);
    creditos.alpha = 0.6;
    var creditos2 = game.add.text(85, game.world.height - 60, "Créditos");
    creditos.width = 160;
    creditos.height = 50;
    creditos.input.useHandCursor = true;

    //  You can drag the pop-up window around
    popup = game.add.sprite(game.world.centerX, game.world.centerY, 'fondo_creditos');
    popup.alpha = 0.95;
    popup.visible = false;
    popup.anchor.set(0.5);
    //popup.inputEnabled = true;
    //popup.input.enableDrag();

    //  Position the close button to the top-right of the popup sprite (minus 8px for spacing)
    var pw = (popup.width / 2) -70;
    var ph = (popup.height / 2) - 30;

    //  And click the close button to close it down again
    var closeButton = game.make.sprite(pw, -ph, 'close');
    closeButton.width = 50;
    closeButton.height = 50;
    closeButton.inputEnabled = true;
    closeButton.input.priorityID = 1;
    closeButton.input.useHandCursor = true;
    closeButton.events.onInputDown.add(this.closeWindow, this);

    //  Add the "close button" to the popup window image
    popup.addChild(closeButton);

    //cebolla
    var logo_creditos = game.make.sprite(-125, -300, 'usb_login_logo_image');
    //closeButton.width = 250;
    //closeButton.height = 250;
    popup.addChild(logo_creditos);

    var coordinacion = game.make.text(-250,-125,"Coordinación Programa Igualdad de Oportunidades");
    game.add.existing(coordinacion);
    coordinacion.width = 500;
    coordinacion.height = 40;
    popup.addChild(coordinacion);


    var hechoPor = game.make.text(-150,50,"Este juego fue realizado por:\nOscar Jiménez\nManuel González\nRicardo Montichelli");
    game.add.existing(hechoPor);
    hechoPor.width = 300;
    hechoPor.height = 120;
    popup.addChild(hechoPor);

    //  Hide it awaiting a click
    popup.scale.set(0.1);

};

Formularium.TitleState.prototype.on_login = function (result) {
    "use strict";
    firebase.database().ref("/users/" + this.game.usuario.value).once("value").then(this.start_game.bind(this));
};

Formularium.TitleState.prototype.start_game = function (snapshot) {
    "use strict";
    var user_data;
    user_data = snapshot.val();

    if (!user_data) {
        this.game.level = 0;
        this.game.starsArray = [0,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4];

    } else {
        this.game.level = user_data.level || 0;
        this.game.starsArray = user_data.starsArray || [0,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4];
    }

    game.global = {
        thumbRows : 5,
        // number of thumbnail cololumns
        thumbCols : 4,
        // width of a thumbnail, in pixels
        thumbWidth : 64,
        // height of a thumbnail, in pixels
        thumbHeight : 64,
        // space among thumbnails, in pixels
        thumbSpacing : 8,
    }
    game.global.starsArray = this.game.starsArray;
    game.global.level = this.game.level;

    this.game.state.start("BootState", true, false, "assets/levels/level_select.json", "LevelSelect");
};

Formularium.TitleState.prototype.openWindow = function () {

    if ((tween !== null && tween.isRunning) || popup.scale.x === 1)
    {
        return;
    }
    popup.visible = true;
    //  Create a tween that will pop-open the window, but only if it's not already tweening or open
    tween = game.add.tween(popup.scale).to( { x: 1, y: 1 }, 1000, Phaser.Easing.Elastic.Out, true);

}

Formularium.TitleState.prototype.closeWindow = function () {

    if (tween && tween.isRunning || popup.scale.x === 0.1)
    {
        return;
    }

    //  Create a tween that will close the window, but only if it's not already tweening or closed
    tween = game.add.tween(popup.scale).to( { x: 0.1, y: 0.1 }, 500, Phaser.Easing.Elastic.In, true);
    popup.visible = false;
}
