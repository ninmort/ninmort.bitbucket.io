var Formularium = Formularium || {};

Formularium.Constelaciones1_tutorial = function () {
    "use strict";
    Phaser.State.call(this);
};

Formularium.Constelaciones1_tutorial.prototype = Object.create(Phaser.State.prototype);
Formularium.Constelaciones1_tutorial.prototype.constructor = Formularium.Constelaciones1_tutorial;

Formularium.Constelaciones1_tutorial.prototype.init = function ()
{
    this.game.scale.setGameSize(1270, 740);
    this.game.scale.pageAlignHorizontally = true;
	this.game.scale.pageAlignVertically = true;
	this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
	this.game.scale.updateLayout();
	//console.log("Prueba");
}

Formularium.Constelaciones1_tutorial.prototype.create = function ()
{
	// game.background = game.add.tileSprite(0, 0, game.world.width, game.world.height, 'spaceBG');
	// game.background.autoScroll(12, 24);
	var dimensions = {
		x: game.width,
		y: game.height
	}

	if (game.width/game.height < 4/3) {

	}

    tutorial = [
        "Bienvenido a Constelaciones",
        "Estas constelaciones tomaron la forma de\n"+
        "un triángulo, y más específicamente de un\n"+
        "triángulo rectángulo. Estos tienen tres\n"+
        "ángulos, y uno de ellos es de 90 grados,\n"+
        "mejor conocido como ángulo recto.",
        "Este tipo de triángulos son usados para\n"+
        "describir una clase de relaciones conocidas\n"+
        "como relaciones trigonométricas. Estas\n"+
        "relacionan uno de los ángulos del triángulo\n"+
        "con los lados del mismo.",
        "Por ejemplo, en este triángulo el lado más\n"+
        "largo se llama hipotenusa y dependiendo del\n"+
        "ángulo (sin contar el ángulo recto) se tiene\n"+
        " un cateto adyacente que está en contacto\n"+
        "con el ángulo y un cateto opuesto que no\n"+
        "toca al ángulo escogido.",
		"Las tres relaciones trigonométricas básicas\n"+
        "que relacionan un ángulo del triángulo\n"+
		"con sus lados son:\n"+
		"Seno, que relaciona el cateto opuesto\n"+
        "respecto a la hipotenusa. \n"+
		"Coseno, que relaciona el cateto adyacente\n"+
        "con la hipotenusa. \n"+
		"Tangente, que relaciona el cateto opuesto\n"+
        "con el cateto adyacente.",
		"¡Es hora de aplicar estos conocimientos a\n"+
        "nuestras constelaciones!\n"+
		"Esta constelación tiene tres ángulos (A, B y C)\ny tres lados (AB, BC y AC).\n"+
		"A continuación aparecerá un enunciado que pide\ncuál de las tres expresiones\n"+ "trigonométricas (seno, coseno o tangente)\nlogra cumplir con los requisitos."
    ];

	game.stage.backgroundColor = '#061f27';
	gameBG = game.add.tileSprite(0, 0, game.width, game.height, 'spaceBG');
	gameBG.autoScroll(game.rnd.integerInRange(-5, 5), game.rnd.integerInRange(-5, 5));
	gameBG.alpha = 0.01*game.rnd.integerInRange(20,50);
	//	INICIALIZACION
	animation = {
		option: false,
		introText: true,
	};
	onCoord = false;		//	booleano para mostrar coordenadas
	star = [];				//	contiene las estrellas
	triangle = [[]];		//	contiene los triangulos
	nTriangles = 1;			//	cantidad de triangulos
	questTriangle = 0;		//	triangulo involucrado con la pregunta
	coord = [];
	options = [];			//	contiene los botones
	questionsPool = [];		//	contiene las preguntas
	choosenQuestions = [];	//	secuencia de preguntas
	iStars = 3;				//	numero inicial de estrellas
	finalStars = 0;			//	numero final de estrellas
	winLevel = false;		//	indicador de nivel ganado
	nOptions = 3;			//	numero de opciones
	fails = 0;				//	cuenta la cantidad de fallos
	addStars = false;		//	agrega nueva estrellas
	var blackStars = 0;
	proportions = {
		x: game.width/1270,
		y: game.height/740,
		min: 0
	};
	if(proportions.x >= proportions.y){
		proportions.min = proportions.y;
	}
	else {
		proportions.min = proportions.x;
	}

	introText = 0;
    lineTween = [];

	nStages = 0;			//	numero de etapas
	stage = 1;				//	etapa actual
	newQuestion = true;	//	nueva pregunta para la nueva etapa

	rotateStars = {
		en: true,			//	habilita las estrellas girando
		theseStars: [],
		num: 0				//	cuantas estrellas giran que es igual a la etapa actual
	};
	lines = [];

    nextButton = {
        button: 0,
        image: 0,
        text: 0,
        currentState : 0,
        totalState: tutorial.length-1
    }

	pauseButton = {
		button: 0,
		click: false,
		bg: 0,
		statusText: 0,
		homeButton: 0,
		helpButton: 0,
		exitButton: 0
	};

	helpState = {
		bg: 0,
		exitButton: 0,
		helpText: 0,
		relations: 0
	};

	gameState = {
		actual: "GAME",
		next: 0
	};
	changeState = false;

	alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'];	//	nombre de las estrellas
	answers = ['seno', 'coseno', 'tangente'];

	color = [0xff0000, 0x00ff00, 0x00ffff];
    pistas = {
        text: this.add.text(game.width*0.8, game.height, '',
			{ font: '20px Trebuchet MS', fill: '#fff', align: 'center' }),
        clue: []
    };
    pistas.text.anchor.setTo(0.5, 1);


	//	NIVEL TUTORIAL
	finalStars = 3;

	//	INICIALIZACION DE ESTRELLAS (UBICACION)
	spotsX = [];	//	almacena posiciones en X
	spotsY = [];	//	almacena posiciones en Y

	nStages = finalStars + 1 - (finalStars/3);		//	numero de etapas es igual al numero de estrellas

	hold = this.shuffle(this.createOrderArray(1, 10));
	spotsX = hold.slice(0, nStages);
	hold = this.shuffle(this.createOrderArray(1, 9));
	spotsY = hold.slice(0, nStages);
	console.log(spotsY);
	spotsY = spotsY.sort();
	console.log(spotsY);

	// propStar = (sortedPos.x[sortedPos.x.length-1] - sortedPos.x[0]);
	// if ((sortedPos.x[sortedPos.x.length-1] - sortedPos.x[0]) > (sortedPos.y[sortedPos.y.length-1] - sortedPos.y[0])) {
	// 	propStar = (sortedPos.x[sortedPos.x.length-1] - sortedPos.x[0]);
	// }
	// else {
	// 	propStar = (sortedPos.y[sortedPos.y.length-1] - sortedPos.y[0]);
	// }

	// hold = this.shuffle(this.createOrderArray(1, 10));
	// spotsX = hold.slice(0, nStages);
	// hold = this.shuffle(this.createOrderArray(1, 10));
	// spotsY = hold.slice(0, nStages);
	// // console.log("spotsX: "+spotsX+" spotsY: "+spotsY);
	// // spotsX = this.notRepeatRandom(8,finalStars);	//	poisicion de las estrellas en X
	// // spotsY = this.notRepeatRandom(8,finalStars);	//	posicion de las estrellas en Y

	//	CONFIGURACION PARA AÑADIR LA TERCERA ESTRELLA
	if ((spotsX[0]>((spotsX[0]+spotsX[1])/2)) && (spotsY[0]>((spotsY[0]+spotsY[1])/2))
		||(spotsX[1]>((spotsX[0]+spotsX[1])/2)) && (spotsY[1]>((spotsY[0]+spotsY[1])/2))){
		hold = spotsX.slice(0,2);						//	obtiene los dos elementos del array
		hold.sort();									//
		spotsX[hold.length] = hold[hold.length-1];
		hold = spotsY.slice(0,2);						//	obtiene los dos elementos del array
		hold.sort();									//
		spotsY[hold.length] = hold[0];
	} else {
		hold = spotsY.slice(0,2);						//	obtiene los dos elementos del array
		hold.sort();									//
		spotsY[hold.length] = hold[0];
		hold = spotsX.slice(0,2);						//	obtiene los dos elementos del array
		hold.sort();									//
		spotsX[hold.length] = hold[0];
	}

	sortedPos = {
		x: spotsX.slice(0).sort(),
		y: spotsY.slice(0).sort(),
	};
	var propStar = (sortedPos.x[sortedPos.x.length-1] - sortedPos.x[0]);
	if (propStar < (sortedPos.y[sortedPos.y.length-1] - sortedPos.y[0] + 1)) {
		propStar = (sortedPos.y[sortedPos.y.length-1] - sortedPos.y[0] + 1);
	}
	// console.log(sortedPos);
	// console.log("propx: "+(sortedPos["x"][2] - sortedPos["x"][0])+" propy: "+(sortedPos["y"][2] - sortedPos["y"][0]));
	//
	// console.log("spotsX: "+spotsX+" spotsY: "+spotsY);


	triangle[0] = [];		//	inicializa el primer triangulo
	// hold = this.shuffle(this.createOrderArray(0, 3));	//	para que sea aleatorio el nombre de las estrellas

	for(var i = 0, j = 0, k = 0; i<finalStars; i++){				//	genera las estrellas * PENDIENTE DEL spotsX.length

		star[i] = {
			x: 0,						//	guarda las coordenadas en X
			y: 0,						//	guarda las coordenadas en Y
			obj: 1,								//	la estrella como asset
			text: 1,							//	el texto que se muestra como leyenda y coordenada
			id: 0,		//	le agrega un nombre
			adj: [],
			op: [],
            emitter: 0
		};

		if ((i == 0)||((i%3)!=0)){

			if(((i%3)!=2)||(i==2)){
				star[i].x = spotsX[k];
				star[i].y = spotsY[k];
				k++;
				console.log("posicion a: "+i);
			}
			star[i].id = alphabet[j];
			rotateStars.theseStars[j] = i;
			j++;
			console.log("letra a :" + i);
		}
		// console.log("i: "+i+" j: "+j+" k: "+k);

		star[i].obj = game.add.sprite(game.height*0.1*star[i].x, game.height*0.1*(10-star[i].y), 'star');
		star[i].obj.anchor.setTo(0.5, 0.5);
		star[i].obj.scale.setTo(0.5*proportions.min, 0.5*proportions.min);
		// star[i].text = this.add.text(star[i].obj.x + 15*proportions.x, star[i].obj.y + 15*proportions.y, '',
		// 	{ font: '20px Trebuchet MS', fill: '#fff', align: 'center' }
		// 	);
		// star[i].text = game.add.bitmapText(star[i].obj.x + 15*proportions.min, star[i].obj.y + 15*proportions.min, "retroFont", "", 20);
        star[i].text = this.add.text(star[i].obj.x + 15*proportions.min, star[i].obj.y + 15*proportions.min, '',
			{ font: '20px Trebuchet MS', fill: '#fff', align: 'center' });
		star[i].text.anchor.setTo(0.5, 0.5);
		star[i].text.scale.setTo(proportions.min, proportions.min);

		star[i].text.visible = false;

		if(i >= iStars){
			star[i].obj.visible = false;
			star[i].text.visible = false;
		}
		else{
			triangle[0][i] = i;
			// star[i].id = alphabet[hold[i]];
		}

		star[i].text.setText(star[i].id);

		// var hold = (star[i].x - sortedPos.x[0])/(sortedPos.x[sortedPos.x.length-1] - sortedPos.x[0]);

		// console.log("star: "+i+" posY: "+star[i].y+" realY: "+star[i].obj.y);

		game.add.tween(star[i].obj).to({x: game.height * ((star[i].x - sortedPos.x[0])/propStar + 0.1)}, 1000, Phaser.Easing.Linear.None, true, 0, 0, false);

		game.add.tween(star[i].obj).to({y: game.height * ((1 - (star[i].y - sortedPos.y[0])/propStar) - 0.1)}, 1000, Phaser.Easing.Linear.None, true, 0, 0, false);

		// var propStar = 1/(sortedPos.x[sortedPos.x.length-1] - sortedPos.x[0] + 1);
		//
		// game.add.tween(star[i].obj).to({x: game.height * propStar * (star[i].x - sortedPos.x[0] + 1) }, 1000, Phaser.Easing.Linear.None, true, 0, 0, false);
		//game.add.tween(star[i].obj).to({y: game.height * propStar * (10 - star[i].y - sortedPos.y[0] + 1) }, 1000, Phaser.Easing.Linear.None, true, 0, 0, false);

	};

	console.log("star length: "+star.length);

	// console.log("length: "+triangle[0].length);

	for(var i = 0; i < triangle[questTriangle].length; i++){			//	asigna cateto opuesto y adjacente

		if (i == (iStars-1)){
			star[i].adj = [triangle[questTriangle][0], triangle[questTriangle][1]];
			star[i].op = star[i].adj;
		}
		else {
			star[i].adj = [triangle[0][i], triangle[0][triangle[0].length-1]];
			hold = triangle[0].slice(0);				//	duplica el array triangle
			hold.splice(i,1);						//	elimina el cateto adjacente del array triangle
			star[i].op = [hold[0], triangle[0][triangle[0].length-1]];
			// console.log(i+" adj: "+star[i].adj+" op: "+star[i].op);
		}
	};

	// introText = this.add.text(game.width*0.5, game.height*0.5, '',
	// 	{ font: '20px Trebuchet MS', fill: '#fff', align: 'center' });
	// introText = game.add.bitmapText(game.width*0.5, game.height*0.5, "retroFont", "Nivel: "+game.level, 35);
    introText = this.add.text( game.width*0.5 , game.height*0.5, 'TUTORIAL',
		{ font: '20px Trebuchet MS', fill: '#fff', align: 'center' });
	introText.anchor.setTo(0.5, 0.5);
	introText.scale.setTo(4*proportions.min, 4*proportions.min);
	// introText.align = "center";

    nextButton.button = game.add.sprite(game.width * 0.8, game.height * 0.9, 'next');
    nextButton.button.anchor.setTo(0.5, 0.5);
    nextButton.button.scale.setTo(proportions.min*0.4, proportions.min*0.4);
    nextButton.button.inputEnabled = true;
    nextButton.button.events.onInputDown.add(this.nextTutorial, this);

    nextButton.text = this.add.text( game.width*0.8 , game.height*0.6, '',
		{ font: '20px Trebuchet MS', fill: '#fff', align: 'center' });
    nextButton.text.anchor.setTo(0.5, 0.5);
    nextButton.text.scale.setTo(proportions.min, proportions.min);
    nextButton.text.setText("¡Bienvenido a Constelaciones!\nPresiona el botón de abajo\npara continuar.\n"+
        nextButton.currentState+"/"+nextButton.totalState);

	nextButton.image = game.add.sprite(game.width*0.8, game.height*0.25, '');
	nextButton.image.anchor.setTo(0.5, 0.5);
	nextButton.image.scale.setTo(0.6, 0.6);

	//	BOTON DE PAUSA
	pauseButton.button = game.add.sprite(game.width, 0, 'pause');
	pauseButton.button.anchor.setTo(1, 0);
	pauseButton.button.scale.setTo(0.1*proportions.min, 0.1*proportions.min);
	pauseButton.button.inputEnabled = true;
	pauseButton.button.events.onInputDown.add(this.changeState, this);

	pauseButton.bg = game.add.sprite(game.width*0.5, game.height*0.5, 'button');
	pauseButton.bg.anchor.setTo(0.5, 0.5);
	pauseButton.bg.scale.setTo(proportions.min, proportions.min);
	pauseButton.bg.visible = false;

	// pauseButton.statusText = this.add.text( game.width*0.5 , game.height*0.5 - 50*proportions.min, '',
	// 	{ font: '20px Trebuchet MS', fill: '#fff', align: 'center' });
	// pauseButton.statusText.anchor.setTo(0.5, 0.5);
	// pauseButton.statusText.scale.setTo(proportions.min, proportions.min);
	// pauseButton.statusText.visible = false;

	pauseButton.homeButton = game.add.sprite(game.width*0.5 , game.height*0.5, 'home');
	pauseButton.homeButton.anchor.setTo(0.5, 0.5);
	pauseButton.homeButton.scale.setTo(0.25*proportions.min, 0.25*proportions.min);
	pauseButton.homeButton.inputEnabled = true;
	pauseButton.homeButton.events.onInputDown.add(this.homeScreen, this);
	pauseButton.homeButton.visible = false;

	pauseButton.exitButton = game.add.sprite(game.width*0.5 + 100*proportions.min, game.height*0.5, 'wrong');
	pauseButton.exitButton.anchor.setTo(0.5, 0.5);
	pauseButton.exitButton.scale.setTo(0.15*proportions.min, 0.15*proportions.min);
	pauseButton.exitButton.events.onInputDown.add(this.changeState, this);
	pauseButton.exitButton.visible = false;
	pauseButton.exitButton.use = "exit";

	pauseButton.helpButton = game.add.sprite(game.width*0.5 - 100*proportions.min, game.height*0.5, 'help');
	pauseButton.helpButton.anchor.setTo(0.5, 0.5);
	pauseButton.helpButton.scale.setTo(0.25*proportions.min, 0.25*proportions.min);
	pauseButton.helpButton.events.onInputDown.add(this.changeState, this);
	pauseButton.helpButton.visible = false;
	pauseButton.helpButton.use = "help";

	helpState.bg = game.add.sprite(game.width*0.5, game.height*0.5, 'button');
	helpState.bg.anchor.setTo(0.5, 0.5);
	helpState.bg.scale.setTo(1.5*proportions.min, 3*proportions.min);
	helpState.bg.visible = false;

	helpState.exitButton = game.add.sprite(game.width*0.5, game.height*0.2, 'wrong');
	helpState.exitButton.anchor.setTo(0.5, 0.5);
	helpState.exitButton.scale.setTo(0.15*proportions.min, 0.15*proportions.min);
	helpState.exitButton.events.onInputDown.add(this.changeState, this);
	helpState.exitButton.visible = false;

	helpState.relations = game.add.sprite(game.width*0.5, game.height*0.5, 'relation');
	helpState.relations.scale.setTo(proportions.min, proportions.min);
	helpState.relations.anchor.setTo(0.5, 0.5);
	helpState.relations.visible = false;
	helpState.relations.tint = "0x000000";

	// helpState.helpText = this.add.text( game.width*0.5 , game.height*0.5, '',
	// 	{ font: '20px Trebuchet MS', fill: '#fff', align: 'center' });
	// helpState.helpText.anchor.setTo(0.5, 0.5);
	// helpState.helpText.scale.setTo(proportions.min, proportions.min);
	// helpState.helpText.visible = false;
	// helpState.helpText.setText("La primera parte del juego consite en: \n" +
	// 	"selecciona la expresion que relacione el angulo y los lados\n" +
	// 	" en la pregunta. \n" +
	// 	"Si la respuesta es correcta, entonces se pasa a la siguiente etapa y \n" +
	// 	"una de las estrellas empezara a girar. Pasaras de nivel cuando todas \n" +
	// 	"las estrellas giren. \n " +
	// 	"Falla dos preguntas y tendras que resolver algo para seguir intentando.");

	// statusText = game.add.bitmapText(game.width*0.8, game.height*0.1, "retroFont", "", 20);
    statusText = this.add.text( game.width*0.8 , 0.05*game.height, '',
		{ font: '20px Trebuchet MS', fill: '#fff', align: 'center' });
	// statusText.text.align = "center";
	// statusText.align = "center";
	// statusText.alpha = 0;
	statusText.anchor.setTo(0.5, 0);
	statusText.setText('Nivel: '+game.level+' Etapa: '+stage+'/'+ nStages + ' Fallos: '+fails);
	statusText.scale.setTo(proportions.x, proportions.y);
	statusText.visible = false;

	//	PREGUNTAS

	// choosenQuestions = this.notRepeatRandom(((iStars-1)*nOptions)-1,iStars);	//	se escogen las preguntas del nivel aleatorio
	choosenQuestions = this.shuffle(this.createOrderArray(0, (iStars-1)*nOptions));
	console.log("choosenQuestion> "+choosenQuestions);

	question = {		//	formato
		num: 0,
		text: this.add.text(
		game.width*0.8, game.height*0.2,
		'', { font: '20px Trebuchet MS', fill: '#fff', align: 'center' }),
	};
	question.text.anchor.setTo(0.5, 0.5);
	question.text.scale.setTo(proportions.min, proportions.min);
	question.text.visible = false;
	question.text.stroke = '#fff';
	// question.text.strokeThickness = 1;
	//	lista de preguntas
	questionsPool[0] = {
		type: '¿Cuál expresión relaciona ',
	};

	for (var i = 0; i < nOptions; i++){		//	genera las opciones

		options[i] = {
			x: game.width*0.8,		//	EDITAR: no tiene sentido
			y: game.height*0.4 + 100*i*proportions.min,
			checkImage: 0,
			button: 0,
			text: 0,
		};

		options[i].button = game.add.sprite(options[i].x, options[i].y, 'button');
		options[i].button.anchor.setTo(0.5, 0.5);
		options[i].button.scale.setTo(0.5*proportions.min, 0.5*proportions.min);
		options[i].button.inputEnabled = true;
		options[i].button.number = i;
        options[i].button.visible = false;
        options[i].button.tint = "0x446600";

		options[i].text = this.add.text(options[i].x, options[i].y, '',
			{ font: '20px Trebuchet MS', fill: '#fff', align: 'center' }
			);
		options[i].text.anchor.setTo(0.5, 0.5);
		options[i].text.setText(answers[i]);
		options[i].text.scale.setTo(proportions.min, proportions.min);
        options[i].text.visible = false;
		options[i].checkImage = game.add.sprite(200, 200, '');	//	QUITAR

	};

	// 	trianLines = [];
	// trianLines[questTriangle] = game.add.graphics(0, 0);
	// trianLines[questTriangle].lineStyle(5, 0xff6600, 0.5);
	// trianLines[questTriangle].moveTo(star[triangle[questTriangle][(triangle[questTriangle].length-1)]].obj.x,
	// 	star[triangle[questTriangle][(triangle[questTriangle].length-1)]].obj.y);
	// trianLines[questTriangle].visible = true;
	//
	// trianLines[questTriangle + finalStars/3] = game.add.graphics(0, 0);
	// trianLines[questTriangle + finalStars/3].lineStyle(2, 0xffffff, 1);
	// trianLines[questTriangle + finalStars/3].moveTo(star[triangle[questTriangle][(triangle[questTriangle].length-1)]].obj.x,
	// 	star[triangle[questTriangle][(triangle[questTriangle].length-1)]].obj.y);
	// trianLines[questTriangle + finalStars/3].visible = true;

	// 	for (var i = 0; i < iStars ; i++){
 // 		trianLines[questTriangle].lineTo(star[triangle[questTriangle][i]].obj.x, star[triangle[questTriangle][i]].obj.y);
 // 		trianLines[questTriangle + finalStars/3].lineTo(star[triangle[questTriangle][i]].obj.x, star[triangle[questTriangle][i]].obj.y);
	// 	}

 	for (var i = 0; i < iStars ; i++){		//	ultimo render sea la estrella
		// 	star[triangle[questTriangle][i]].obj.bringToTop();
 		star[triangle[questTriangle][i]].obj.alpha = 0;
 		game.add.tween(star[triangle[questTriangle][i]].obj).to({alpha: 1}, 1000, 'Linear', true);
 		hold = game.rnd.integerInRange(10, 30);
 		game.add.tween(star[triangle[questTriangle][i]].obj.scale).to({x: 0.5 + hold*0.01, y: 0.5 + hold*0.01}, 1000 + game.rnd.integerInRange(0, 500), 'Linear', true, game.rnd.integerInRange(0, 200), -1, true);
 		// star[triangle[questTriangle][i]].text.bringToTop();
 	}

	// 	game.add.tween(trianLines[questTriangle + finalStars/3]).to({alpha: 0}, 1000, "Linear", true, 0, -1, true);

 	if(finalStars/3 > (questTriangle+1)){
 		addStars = true;
 	}

	// while(addStars){
	//
	// 	questTriangle++;
	// 	// addStars = false;
	// 	triangle[questTriangle] = [];
	//
	// 	// hold = this.shuffle(this.createOrderArray(0, 3));	//	aleatorio entre las estrellas del triangulo previo
	// 	hold = [];
	// 	if (star[triangle[questTriangle-1][0]].y > star[triangle[questTriangle-1][1]].y){
	// 		hold[0] = 0;
	// 	} else {
	// 		hold[0] = 1;
	// 	}
	// 	// console.log("nombre star repeat: "+star[triangle[questTriangle-1][hold]].id);
	// 	triangle[questTriangle][0] = questTriangle*3;
	// 	star[triangle[questTriangle][0]].id = star[triangle[questTriangle-1][hold[0]]].id;
	// 	star[triangle[questTriangle][0]].x = star[triangle[questTriangle-1][hold[0]]].x;
	// 	star[triangle[questTriangle][0]].y = star[triangle[questTriangle-1][hold[0]]].y;
	// 	star[triangle[questTriangle][0]].obj.x = star[triangle[questTriangle-1][hold[0]]].obj.x;
	// 	star[triangle[questTriangle][0]].obj.y = star[triangle[questTriangle-1][hold[0]]].obj.y;
	//
	// 	triangle[questTriangle][1] = 1 + questTriangle*3;
	// 	star[triangle[questTriangle][1]].obj.visible = false;
	// 	star[triangle[questTriangle][1]].text.visible = false;
	//
	// 	triangle[questTriangle][2] = 2 + questTriangle*3;
	// 	star[triangle[questTriangle][2]].x = star[triangle[questTriangle][0]].x;		//	MODIFICAR
	// 	star[triangle[questTriangle][2]].y = star[triangle[questTriangle][1]].y;		//	MODIFICAR
	// 	star[triangle[questTriangle][2]].obj.x = star[triangle[questTriangle][2]].x*game.height*0.1;
	// 	star[triangle[questTriangle][2]].obj.y = (10-star[triangle[questTriangle][2]].y)*game.height*0.1;
	// 	star[triangle[questTriangle][2]].text.x = star[triangle[questTriangle][2]].obj.x + 15*proportions.min;
	// 	star[triangle[questTriangle][2]].text.y = star[triangle[questTriangle][2]].obj.y + 15*proportions.min;
	// 	star[triangle[questTriangle][2]].obj.visible = false;
	// 	star[triangle[questTriangle][2]].text.visible = false;
	//
	// 	for(var i = 0; i<triangle[questTriangle].length-1; i++){			//	asigna cateto opuesto y adjacente
	// 		if (i == triangle[questTriangle].length-1){
	// 			star[triangle[questTriangle][i]].adj = [triangle[questTriangle][0], triangle[questTriangle][1]];
	// 			star[triangle[questTriangle][i]].op = [triangle[questTriangle][0], triangle[questTriangle][1]];
	// 		}
	// 		else {
	// 			star[triangle[questTriangle][i]].adj = [triangle[questTriangle][i], triangle[questTriangle][triangle[questTriangle].length-1]];
	// 			hold = triangle[questTriangle].slice(0);				//	duplica el array triangle
	// 			hold.splice(i,1);						//	elimina el cateto adjacente del array triangle
	// 			star[triangle[questTriangle][i]].op = [hold[0], triangle[questTriangle][triangle[questTriangle].length-1]];
	// 		}
	// 		// console.log(triangle[0][i]+" : adj: "+star[triangle[0][i]].adj + " op: " + star[triangle[0][i]].op);
	// 	};
	//
	// 	trianLines[questTriangle] = game.add.graphics(0, 0);
	// 	trianLines[questTriangle].lineStyle(5, 0xff6600, 0.5);
	// 	trianLines[questTriangle].moveTo(star[triangle[questTriangle][(triangle[questTriangle].length-1)]].obj.x,
	// 		star[triangle[questTriangle][(triangle[questTriangle].length-1)]].obj.y);
	//
	// 	trianLines[questTriangle + finalStars/3] = game.add.graphics(0, 0);
	// 	trianLines[questTriangle + finalStars/3].lineStyle(2, 0xffffff, 1);
	// 	trianLines[questTriangle + finalStars/3].moveTo(star[triangle[questTriangle][(triangle[questTriangle].length-1)]].obj.x,
	// 		star[triangle[questTriangle][(triangle[questTriangle].length-1)]].obj.y);
	//
	//  	for (var i = 0; i < iStars ; i++){
	//  		trianLines[questTriangle].lineTo(star[triangle[questTriangle][i]].obj.x, star[triangle[questTriangle][i]].obj.y);
	//  		trianLines[questTriangle + finalStars/3].lineTo(star[triangle[questTriangle][i]].obj.x, star[triangle[questTriangle][i]].obj.y);
	//  	}
	//
	//  	for (var i = 0; i < iStars ; i++){		//	ultimo render sea la estrella
	//  		star[triangle[questTriangle][i]].obj.bringToTop();
	//  		star[triangle[questTriangle][i]].obj.alpha = 0;
	//  		game.add.tween(star[triangle[questTriangle][i]].obj).to({alpha: 1}, 1000, 'Linear', true);
	//  		hold = game.rnd.integerInRange(0, 30);
	//  		game.add.tween(star[triangle[questTriangle][i]].obj.scale).to({x: 0.5 + hold*0.01, y: 0.5 + hold*0.01}, 1000 + game.rnd.integerInRange(0, 500), 'Linear', true, game.rnd.integerInRange(0, 200), -1, true);
	//  		// star[triangle[questTriangle][i]].text.bringToTop();
	//  	}
	//
	//  	trianLines[questTriangle].visible = false;
	//  	trianLines[questTriangle + finalStars/3].visible = false;
	//  	game.add.tween(trianLines[questTriangle + finalStars/3]).to({alpha: 0}, 1000, "Linear", true, 0, -1, true);
	//
	//  	if(finalStars/3 == (questTriangle+1)){
 // 			addStars = false;
 // 		}
	// }

	questTriangle = 0;

	for (var i = 0; i < star.length ; i++){		//	ultimo render sea la estrella
	 	star[i].obj.bringToTop();
		// star[triangle[questTriangle][i]].text.bringToTop();
	 }

	// choosenQuestions.shift();		//	elimina la primera pregunta ya hecha, sigue la secuencia
	var intro = game.add.tween(introText).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true, 0, 0, false);
    intro.onComplete.add(function() {
		trianLines = [];
		for (var i = 0, n = triangle.length; i < n; i++) {
			trianLines[i] = game.add.graphics(0, 0);
			trianLines[i].lineStyle(5, 0xff6600, 0.5);
			trianLines[i].moveTo(star[triangle[i][(triangle[i].length-1)]].obj.x,
				star[triangle[i][(triangle[i].length-1)]].obj.y);
			// trianLines[questTriangle].visible = true;

			trianLines[i + finalStars/3] = game.add.graphics(0, 0);
			trianLines[i + finalStars/3].lineStyle(2, 0xffffff, 1);
			trianLines[i + finalStars/3].moveTo(star[triangle[i][(triangle[i].length-1)]].obj.x,
				star[triangle[i][(triangle[i].length-1)]].obj.y);
			trianLines[i + finalStars/3].visible = true;

			for (var j = 0; j < 3; j++) {
				star[triangle[i][j]].text.x = star[triangle[i][j]].obj.x + 30*proportions.min;
				star[triangle[i][j]].text.y = star[triangle[i][j]].obj.y + 30*proportions.min;
				star[triangle[i][j]].text.visible = true;
				// star[triangle[i][j]].text.alpha = 0;
				// game.add.tween(star[triangle[i][j]].text).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true, 0, 0, false);

				trianLines[i].lineStyle(5, color[j], 0.5);
				trianLines[i].lineTo(star[triangle[i][j]].obj.x, star[triangle[i][j]].obj.y);
				trianLines[i + finalStars/3].lineTo(star[triangle[i][j]].obj.x, star[triangle[i][j]].obj.y);

                star[triangle[i][j]].emitter = game.add.emitter(star[triangle[i][j]].obj.x, star[triangle[i][j]].obj.y, 100);
                star[triangle[i][j]].emitter.makeParticles('star');
                star[triangle[i][j]].emitter.minParticleSpeed.setTo(-400, -400);
                star[triangle[i][j]].emitter.maxParticleSpeed.setTo(400, 400);
                star[triangle[i][j]].emitter.maxParticleScale = 0.05;
                star[triangle[i][j]].emitter.maxParticleAlpha = 0.1;
                star[triangle[i][j]].emitter.gravity = 0;

				star[triangle[i][j]].obj.bringToTop();
				star[triangle[i][j]].text.bringToTop();
			}

			trianLines[i].alpha = 0;

            lineTween[i] = game.add.tween(trianLines[i]).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true, 0, 0, false);
            // lineTween[i].pause();

			lineTween[i + finalStars/3] = game.add.tween(trianLines[i + finalStars/3]).to({alpha: 0}, 1000, "Linear", true, 0, -1, true);
            // lineTween[i + finalStars/3].pause();

		};

        // star.map(function (singleStar) {
        //     singleStar.obj.alpha = 0.2;
        // })
	},this);
	// intro.onComplete.add(function() {
    //
	// 	trianLines = [];
	// 	trianLines[questTriangle] = game.add.graphics(0, 0);
	// 	trianLines[questTriangle].lineStyle(5, 0xff6600, 0.5);
	// 	trianLines[questTriangle].moveTo(star[triangle[questTriangle][(triangle[questTriangle].length-1)]].obj.x,
	// 		star[triangle[questTriangle][(triangle[questTriangle].length-1)]].obj.y);
	// 	trianLines[questTriangle].visible = true;
    //
	// 	trianLines[questTriangle + finalStars/3] = game.add.graphics(0, 0);
	// 	trianLines[questTriangle + finalStars/3].lineStyle(2, 0xffffff, 1);
	// 	trianLines[questTriangle + finalStars/3].moveTo(star[triangle[questTriangle][(triangle[questTriangle].length-1)]].obj.x,
	// 		star[triangle[questTriangle][(triangle[questTriangle].length-1)]].obj.y);
	// 	trianLines[questTriangle + finalStars/3].visible = true;
    //
	// 	for (var i = 0, n = star.length; i < n; i++) {
	// 		star[i].text.x = star[i].obj.x + 30*proportions.min;
	// 		star[i].text.y = star[i].obj.y + 30*proportions.min;
	// 		star[i].text.visible = true;
	// 		star[i].text.alpha = 0;
	// 		game.add.tween(star[i].text).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true, 0, 0, false);
    //
	// 		trianLines[questTriangle].lineStyle(5, color[i], 0.5);
	// 		trianLines[questTriangle].lineTo(star[i].obj.x, star[i].obj.y);
	// 		trianLines[questTriangle + finalStars/3].lineTo(star[i].obj.x, star[i].obj.y);
    //
    //
	// 		star[i].obj.bringToTop();
	// 		star[i].text.bringToTop();
	// 	}
    //
	// 	game.add.tween(trianLines[questTriangle + finalStars/3]).to({alpha: 0}, 1000, "Linear", true, 0, -1, true);
	// 	// console.log("questTriangle "+questTriangle+" triangle "+triangle[questTriangle]);
    //
	// 	// trianLines[questTriangle].lineTo(star[triangle[questTriangle][i]].obj.x, star[triangle[questTriangle][i]].obj.y);
	// 	//  	 	trianLines[questTriangle + finalStars/3].lineTo(star[triangle[questTriangle][i]].obj.x, star[triangle[questTriangle][i]].obj.y);
	// }, this);
	// var intro = game.add.tween(statusText).to({alpha: 1}, 1000, Phaser.Easing.Linear.None, true, 0, 0, false);
	// game.add.tween(trianLines[questTriangle + finalStars/3]).to({alpha: 0}, 1000, "Linear", true, 0, -1, true);
	// game.add.tween(trianLines[questTriangle + finalStars/3]).to({lineWidth: 0}, 1000, "Linear", true, 0, -1, true);
},

Formularium.Constelaciones1_tutorial.prototype.update = function ()
{
	if (changeState){
		changeState = false;

		if (gameState.actual == "GAME"){

			gameState.actual = "PAUSE";
			pauseButton.bg.visible = true;
			pauseButton.bg.bringToTop();
			// pauseButton.statusText.visible = true;
			// pauseButton.statusText.setText('Level> '+game.level+' Stage> '+stage+' Fails> '+fails);
			// pauseButton.statusText.bringToTop();
			pauseButton.homeButton.visible = true;
			pauseButton.homeButton.bringToTop();
			pauseButton.exitButton.visible = true;
			pauseButton.exitButton.bringToTop();
			pauseButton.exitButton.inputEnabled = true;
			pauseButton.helpButton.visible = true;
			pauseButton.helpButton.bringToTop();
			pauseButton.helpButton.inputEnabled = true;

		}
		else if (gameState.actual == "PAUSE"){

			gameState.actual = "GAME";
			pauseButton.bg.visible = false;
			// pauseButton.statusText.visible = false;
			pauseButton.homeButton.visible = false;
			pauseButton.exitButton.visible = false;
			pauseButton.helpButton.visible = false;


			if(gameState.next == "HELP"){

				gameState.actual = "HELP";
				helpState.bg.visible = true;
				helpState.bg.bringToTop();
				helpState.exitButton.visible = true;
				helpState.exitButton.inputEnabled = true;
				helpState.exitButton.bringToTop();
				// helpState.helpText.visible = true;
				// helpState.helpText.bringToTop();
				helpState.relations.visible = true;
				helpState.relations.bringToTop();


			}
			else {
				pauseButton.button.inputEnabled = true;
			}
		}
		else if (gameState.actual == "HELP"){

			gameState.actual = "GAME";
			helpState.bg.visible = false;
			helpState.exitButton.visible = false;
			// helpState.helpText.visible = false;
			helpState.relations.visible = false;
			pauseButton.button.inputEnabled = true;
		}

		console.log("GameState: "+gameState.actual);
	}

	if (newQuestion){

		newQuestion = false;
		// animation.option = true;
		question.text.setText('');

		for (var i=0; i<nOptions; i++){					//	elimina las opciones previas
			options[i].button.events.onInputDown.removeAll();
			options[i].button.inputEnabled = true;
			options[i].checkImage.destroy();
			options[i].button.x = game.width + options[i].button.width;
			options[i].text.x = options[i].button.x;
			question.text.x = options[i].button.x;
			game.add.tween(options[i].button).to({x: game.width*0.8}, 500, Phaser.Easing.Linear.None, true, 0, 0);
			game.add.tween(options[i].text).to({x: game.width*0.8}, 500, Phaser.Easing.Linear.None, true, 0, 0);
		}
		game.add.tween(question.text).to({x: game.width*0.8}, 500, Phaser.Easing.Linear.None, true, 0, 0);

        star.map(function(ra) {
            ra.obj.tint = "0xffffff";
        });
									//	SE PASO A LA SIGUIENTE ETAPA

		if (winLevel){						//	GANAR
			winLevel = false;
			// nextLvlButton = this.add.button(game.width*0.8, game.height*0.5,
			//  'button', this.nextLevel, this);
			// nextLvlButton.anchor.setTo(0.5, 0.5);
			// nextLvlButton.scale.setTo(0.5*proportions.min, 0.5*proportions.min);
			// nextLvlButton.text = this.add.text(nextLvlButton.x, nextLvlButton.y, '',
			// 	{ font: '20px Trebuchet MS', fill: '#fff', align: 'center' });
			// nextLvlButton.text.anchor.setTo(0.5, 0.5);
			// nextLvlButton.text.setText('Siguiente');
			// nextLvlButton.text.scale.setTo(proportions.min, proportions.min);
            this.resultState();
			// helpButton.destroy();

			for(var i = 0; i < nOptions ; i++){
				options[i].button.destroy();
				options[i].text.destroy();
			}
		} else {

			if(addStars){

				//	FORMAR UN NUEVO TRIANGULO
				questTriangle++;
				addStars = false;
				for(var i = 1; i < triangle[questTriangle].length; i++){
					star[triangle[questTriangle][i]].obj.visible = true;
					star[triangle[questTriangle][i]].text.visible = true;
				}
				trianLines[questTriangle].visible = true;
				trianLines[questTriangle + finalStars/3].visible = true;
			 	//	NUEVAS PREGUNTAS DEBIDO A NUEVO TRIANGULO
				// console.log("choosenQuestion: "+choosenQuestions);
				hold = choosenQuestions.concat(this.createOrderArray(6*questTriangle, 6*(questTriangle+1)));
				// console.log("choosenQuestion: "+hold);
				choosenQuestions = this.shuffle(hold);

				// choosenQuestions = this.shuffle(this.createOrderArray(6*questTriangle, 6*(questTriangle+1)));
				console.log("choosenQuestion: "+choosenQuestions);

			}

			if((choosenQuestions[0]%6)<2){
				// console.log("op "+star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].op);
				correctOption = 0;		//	seno
				question.text.setText(questionsPool[0].type+
					"\nel ángulo "+star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].id+
					"\ny los lados "+star[star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].op[0]].id+
					star[star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].op[1]].id+
					" y "+star[triangle[Math.floor(choosenQuestions[0]/6)][0]].id+star[triangle[Math.floor(choosenQuestions[0]/6)][1]].id+"?");

				let catetoColor = (star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].op[0]) ? '#0aa' : '#f00';
				question.text.addColor(catetoColor, 48);
				question.text.addColor('#fff', 51);
				question.text.addColor('#0f0', 53);
				question.text.addColor('#fff', 56);

                pistas.lista = [];

                pistas.lista.push("Acuerdate que para el ángulo " + star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].id + ",\n" +
                "el cateto opuesto es " + star[star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].op[0]].id+
                star[star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].op[1]].id);

                pistas.lista.push("Acuerdate que para el ángulo " + star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].id + ",\n" +
                "la hipotenusa es " + star[triangle[Math.floor(choosenQuestions[0]/6)][0]].id+star[triangle[Math.floor(choosenQuestions[0]/6)][1]].id);

			}
			else if((choosenQuestions[0]%6)<4){
				// console.log("adj "+triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]);
				correctOption = 1;		//	coseno
				question.text.setText(questionsPool[0].type+
					"\nel ángulo "+star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].id+
					"\ny los lados "+star[star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].adj[0]].id+
					star[star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].adj[1]].id+
					" y "+star[triangle[Math.floor(choosenQuestions[0]/6)][0]].id+star[triangle[Math.floor(choosenQuestions[0]/6)][1]].id+"?");

				let catetoColor = (star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].adj[0]) ? '#0aa' : '#f00';
				question.text.addColor(catetoColor, 48);
				question.text.addColor('#fff', 51);
				question.text.addColor('#0f0', 53);
				question.text.addColor('#fff', 56);
				// question.text.addColor('#ff0000', 5);
				// question.text.addColor('#ffffff', 50);
				// question.text.addColor(color[1], 54);

                pistas.lista = [];

                pistas.lista.push("Acuerdate que para el ángulo " + star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].id + ",\n" +
                "el cateto adyacente es " + star[star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].adj[0]].id+
                star[star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].adj[1]].id);

                pistas.lista.push("Acuerdate que para el ángulo " + star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].id + ",\n" +
                "la hipotenusa es " + star[triangle[Math.floor(choosenQuestions[0]/6)][0]].id+star[triangle[Math.floor(choosenQuestions[0]/6)][1]].id);
			}
			else{
				correctOption = 2;		//	tangente
				question.text.setText(questionsPool[0].type+
					"\nel ángulo "+star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].id+
					"\ny los lados "+star[star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].op[0]].id+
					star[star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].op[1]].id+
					" y "+star[star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].adj[0]].id+star[star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].adj[1]].id+"?");

				// question.text.addColor('#ff0000', 5);
				// question.text.addColor('0xffffff', 50);
				// question.text.addColor(color[1], 54);
				let catetoColor = (star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].op[0]) ? '#0aa' : '#f00';
				question.text.addColor(catetoColor, 48);
				question.text.addColor('#fff', 51);
				catetoColor = (star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].adj[0]) ? '#0aa' : '#f00';
				question.text.addColor(catetoColor, 53);
				question.text.addColor('#fff', 56);

                pistas.lista = [];

                pistas.lista.push("Acuerdate que para el ángulo " + star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].id + ",\n" +
                "el cateto adyacente es " + star[star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].adj[0]].id+
                star[star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].adj[1]].id);

                pistas.lista.push("Acuerdate que para el ángulo " + star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].id + ",\n" +
                "el cateto opuesto es " + star[star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].op[0]].id+
                star[star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].op[1]].id);
			};

            pistas.lista.push(
                "Acuerdate que las expresiones\n"+
                "trigonométricas se encuentran en\n"+
                "el menú de ayuda que se encuentra\n"+
                " al darle al botón de pausa");
            pistas.lista.push(
                "Acuerdate que la constelación\n"+
                "esta pintada de cierta forma,\n"+
                "el ángulo relativo a la pregunta\n"+
                "está de amarillo");
            pistas.lista.push(
                "Acuerdate que la constelación\n"+
                "esta pintada de cierta forma,\n"+
                "el color de los lados del enunciado\n"+
                "corresponden a los colores de los\n"+
                "trazos entre las estrellas");

            pistas.lista = this.shuffle(pistas.lista);
            pistas.text.setText("");

            star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].obj.tint = "0xffff00";

			hold = this.shuffle(this.createOrderArray(0, answers.length));

			for (var i = 0; i < nOptions ; i++){
				options[i].text.setText(answers[hold[i]]);
				if (hold[i] != correctOption){
					//	OPCION INCORRECTA
					options[i].button.events.onInputDown.add(this.wrongOption,this);
					console.log("wrong: "+i);
				}
				else{
					//	OPCION CORRECTA
					options[i].button.events.onInputDown.add(this.rightOption,this);
					console.log("good: "+i);
				}
			};
		};
	};


	if(rotateStars.en){
		for(var i=0; i <rotateStars.num; i++){
			star[rotateStars.theseStars[i]].obj.angle += 1;
		};
	};

},

//	genera una matriz de dimension numbersNeeded de aleatorios no repetidos contando el 0 hasta el topLimit
Formularium.Constelaciones1_tutorial.prototype.notRepeatRandom = function (topLimit, numbersNeeded)
{
	for (var i = 0, ar = []; i < topLimit; i++) {
	    ar[i] = i+1;
	}

	ar.sort(function () {
	    return Math.random() - 0.5;
	});
	var numbersNotRepeated = [];
	for (var i = 0; i<numbersNeeded; i++){
		numbersNotRepeated.push(ar[ar.length-1]);
		ar.pop();
	}
	return numbersNotRepeated;
},

Formularium.Constelaciones1_tutorial.prototype.toggleCoordenates = function (star)
{

	if(onCoord){
		for(var i = 0;i<iStars; i++){
			star[i].text.setText(star[i].id);
		};
		onCoord = false;
	}
	else{
		for(var i = 0; i<iStars; i++){
			star[i].text.setText(star[i].id+':( '+star[i].x+', '+star[i].y+')');
		};
		onCoord = true;
	};
},

Formularium.Constelaciones1_tutorial.prototype.wrongOption = function (boton)
{
	options[boton.number].checkImage = game.add.sprite(options[boton.number].x + 200*proportions.min, options[boton.number].y, 'wrong');
	options[boton.number].checkImage.anchor.setTo(0.5,0.5);
	options[boton.number].checkImage.scale.setTo(0.15*proportions.min,0.15*proportions.min);
	fails++;
	boton.inputEnabled = false;
	statusText.setText('Nivel: '+game.level+' Etapa: '+stage+'/'+ nStages + ' Fallos: '+fails);
	// pauseButton.statusText.setText('Level> '+game.level+' Stage> '+stage+'\nFails> '+fails);
    let hold = pistas.lista.shift();
    pistas.text.setText(hold);
    pistas.lista.push(hold);
},

Formularium.Constelaciones1_tutorial.prototype.rightOption = function (boton)
{

	options[boton.number].checkImage = game.add.sprite(options[boton.number].x + 200*proportions.min, options[boton.number].y, 'right');
	options[boton.number].checkImage.anchor.setTo(0.5*proportions.min,0.5*proportions.min);
	options[boton.number].checkImage.scale.setTo(0.3,0.3);

	if(stage == nStages){

		winLevel = true;					//	GANAR

	} else {
		console.log("stage "+stage+" iStars "+iStars+" nStages "+nStages);
		if ((stage == iStars)&&(stage < nStages)){

			console.log("mas estrellas");
			addStars = true;
			iStars+=2;

		}

		stage++;
		choosenQuestions.shift();		//	elimina la primera pregunta ya hecha, sigue la secuencia
	}

    star[rotateStars.theseStars[rotateStars.num]].emitter.start(false, 200, 100);
	rotateStars.num++;
	// pauseButton.statusText.setText('Level> '+game.level+' Stage> '+stage+'\nFails> '+fails);
	newQuestion = true;
	statusText.setText('Nivel: '+game.level+' Etapa: '+stage+'/'+ nStages + ' Fallos: '+fails);

},

Formularium.Constelaciones1_tutorial.prototype.nextLevel = function ()
{
	//YO MANUEL COMENTE ESTAS LINEAS
	// game.level++;
	// this.state.start('Parte1');
	var estrellas = 3-blackStars;
	if(game.global.starsArray[game.global.level-1]<estrellas)
	{
		game.global.starsArray[game.global.level-1] = estrellas;
		firebase.database().ref("/users/" + this.game.usuario.value + "/starsArray").set(game.global.starsArray);
	}
	let change = false;
	// if we completed a level and next level is locked - and exists - then unlock it
	if(estrellas>0 && game.global.starsArray[game.global.level]==4 && game.global.level<game.global.starsArray.length)
	{
		game.global.starsArray[game.global.level] = 0;
		change = true;
	}
	if (estrellas>0 && game.global.starsArray[game.global.level+3]==4 && game.global.level<game.global.starsArray.length-4)
	{
		game.global.starsArray[game.global.level+3] = 0;
		change = true;
	}
	if (change == true)
	{
		change = false;
		firebase.database().ref("/users/" + this.game.usuario.value + "/starsArray").set(game.global.starsArray);
	}
	this.game.state.start("BootState", true, false, "assets/levels/level_select.json", "LevelSelect");
},

Formularium.Constelaciones1_tutorial.prototype.homeScreen = function ()
{
	//YO MANUEL COMENTE ESTA LINEA
	//this.state.start('Menu');
	this.game.state.start("BootState", true, false, "assets/levels/level_select.json", "LevelSelect");
},

Formularium.Constelaciones1_tutorial.prototype.shuffle = function (array)
{

	var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
	while (0 !== currentIndex) {

	    // Pick a remaining element...
	    randomIndex = Math.floor(Math.random() * currentIndex);
	    currentIndex -= 1;

	    // And swap it with the current element.
	    temporaryValue = array[currentIndex];
	    array[currentIndex] = array[randomIndex];
	    array[randomIndex] = temporaryValue;
  	}

	return array;
},

Formularium.Constelaciones1_tutorial.prototype.createOrderArray = function (init, finish)
{
	var array = [];
	for (var i = init; i < finish; i++){
		array[i - init] = i;
	}
	return array;
},

Formularium.Constelaciones1_tutorial.prototype.changeState = function (buton)
{

	changeState = true;
	buton.inputEnabled = false;
	if (buton.use == "help"){
		console.log(buton.use);
		gameState.next = "HELP";
	}
	else if (buton.use == "exit"){
		console.log(buton.use);
		gameState.next = "GAME";
	}
},

Formularium.Constelaciones1_tutorial.prototype.nextTutorial = function ()
{
    if (nextButton.currentState < nextButton.totalState) {
        nextButton.currentState++;
        nextButton.text.setText(tutorial[nextButton.currentState]+"\n"+
            nextButton.currentState+" \\ "+nextButton.totalState);
		// if (nextButton.currentState == 1) {
		// 	nextButton.image.loadTexture('rectTri');
		// }
		if (nextButton.currentState == 1) {
			nextButton.image.loadTexture('relation');
			nextButton.image.scale.setTo(proportions.min*0.5, proportions.min*0.5);
		}
    } else {
        nextButton.text.visible = false;
        nextButton.button.visible = false;
		nextButton.image.visible = false;

		statusText.visible = true;
		question.text.visible = true;
		for (var i = 0, n = options.length; i < n; i++) {
			options[i].button.visible = true;
			options[i].text.visible = true;
		}
    }
},

Formularium.Constelaciones1_tutorial.prototype.resultState = function () {
    let resultUI = {
        bg: 0,
        star: []
    };

    blackStars = fails;
    if (fails >= 1) {
        blackStars = 1;
    }
    if (fails >= 3) {
        blackStars = 2;
    }
    if (fails >= 6) {
        blackStars = 3;
    }

    resultUI.bg = game.add.sprite(game.width * 0.5, game.height *0.5, 'button');
    resultUI.bg.scale.setTo(proportions.min, 2*proportions.min);
    resultUI.bg.anchor.setTo(0.5);

    nextLvlButton = this.add.button(game.width*0.5, game.height*0.6, 'next', this.nextLevel, this);
    nextLvlButton.anchor.setTo(0.5, 0.5);
    nextLvlButton.scale.setTo(0.5*proportions.min);

    for (let i = 0; i < 3; i++) {
        resultUI.star[i] = game.add.sprite(game.width * 0.1 * (4 + i), game.height * (0.5 - ((i%2) * 0.1)), 'starScore');
        resultUI.star[i].scale.setTo(proportions.min*0.5);
        resultUI.star[i].anchor.setTo(0.5);
        if (3 - blackStars <= i) {
            resultUI.star[i].tint = "0x666666";
        }
    }
}
