
var firstCardClicked = false;
var secondCardClicked = false;
var firstCardValue,secondCardValue;
var timeCheck = 0;
var flippedCards;
var facingCards;
var cards;
var boardWidth,boardHeight,horizontalSpacing,verticalSpacing,OffsetX,OffsetY,sc;
var cardList;
var emitter;
var inPlay;
var cardGroup;
var fxGroup;
var textGroup;

var timesUp;
var myCountdownSeconds;
var timeCheck = 0;
var timeLimit;
var mySeconds;

var Formularium = Formularium || {};

Formularium.Memorium_tutorial = function () {
    "use strict";
    Phaser.State.call(this);
};

Formularium.Memorium_tutorial.prototype = Object.create(Phaser.State.prototype);
Formularium.Memorium_tutorial.prototype.constructor = Formularium.Memorium_tutorial;

Formularium.Memorium_tutorial.prototype.init = function ()
{
	//Modifico el tamano de la pantalla
	// this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
 //    this.game.scale.minWidth = 480;
 //    this.game.scale.minHeight = 260;
 //    this.game.scale.maxWidth = 1024;
 //    this.game.scale.maxHeight = 768;
 //    this.game.scale.pageAlignHorizontally = true;
 //    this.game.scale.pageAlignVertically = true;
    //this.game.scale.setScreenSize(true);

    this.game.scale.setGameSize(1024, 768);
    this.game.scale.pageAlignHorizontally = true;
	this.game.scale.pageAlignVertically = true;
	this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
	this.game.scale.updateLayout();
	//console.log("Prueba");
}

Formularium.Memorium_tutorial.prototype.create = function ()
{

    tutorial = [
        "Bienvenido a Memorium!",
        "Como cualquier juego de memoria, consiste en\nbuscar las parejas que coincidan.\n"+
        "Pero en este caso las cartas contienen parte\nde las expresiones trigonométricas\n"+
        "más conocidas. ¡Busca todas las parejas para\npasar de nivel!",
        "Si tienes alguna duda sobre las expresiones,\nconsulta la tabla que contiene las\n"+
        "identidades en el menú de ayuda."
    ];

    nextButton = {
        button: 0,
        image: 0,
        text: 0,
        currentState : 0,
        totalState: tutorial.length-1
    }

	//create in game arrays
	cardList = [];
	cardList2 = [];
	inPlay =[];
	this.game.add.sprite(0,0,'background');

	// create groups
	cardGroup = this.game.add.group();
	fxGroup = this.game.add.group();

	// game play audio
	button = this.add.audio('button',1,false);
	cardFlip = this.add.audio('cardFlip',1,false);
	correct = this.add.audio('correct',1,false);
	wrong = this.add.audio('wrong',1,false);
	fanfair = this.add.audio('fanfair',1,false);
	roar = this.add.audio('roar',1,false);

	//decide which level to load
	switch(game.global.nivel_memoria){

		case 'easy':
			boardWidth = 3;
			boardHeight = 2;
			horizontalSpacing = 240;
			verticalSpacing = 240;
			OffsetX = 280;
			OffsetY = 260;
			timeLimit = 10;

		break;

		case 'medium':
			boardWidth = 4;
			boardHeight = 2;
			horizontalSpacing = 240;
			verticalSpacing = 240;
			OffsetX = 150;
			OffsetY = 260;
			timeLimit = 18;

		break;

		case 'hard':
			boardWidth = 4;
			boardHeight = 3;
			horizontalSpacing = 210;
			verticalSpacing = 240;
			OffsetX = 200;
			OffsetY = 140;
			timeLimit = 25;

		break;

	}

	//create the level
	this.buildCards(boardHeight,boardWidth,horizontalSpacing,verticalSpacing,OffsetX,OffsetY,sc);

	//Create the Timer
	//this.timer(timeLimit, mySeconds);
	// this.update(timeLimit);

	//create emitters for correct display
	emitter = this.game.add.emitter(0, 0, 200);
	emitter.makeParticles('star');
	emitter.gravity = 0;
	emitter.minParticleScale = 0.1;
	emitter.maxParticleScale = 0.5;
	emitter.setAll('anchor.x', 0.5);
    emitter.setAll('anchor.y', 0.5);
	emitter.minParticleSpeed.setTo(-200, -200);
    emitter.maxParticleSpeed.setTo(400, 400);
	fxGroup.add(emitter);


	emitter2 = this.game.add.emitter(0, 0, 200);
	emitter2.makeParticles('star');
	emitter2.gravity = 0;
	emitter2.minParticleScale = 0.1;
	emitter2.maxParticleScale = 0.5;
	emitter2.setAll('anchor.x', 0.5);
    emitter2.setAll('anchor.y', 0.5);
	emitter2.minParticleSpeed.setTo(-200, -200);
    emitter2.maxParticleSpeed.setTo(400, 400);
	fxGroup.add(emitter2);







	proportions = {
		x: game.width/1270,
		y: game.height/740,
		min: 0
	};
	if(proportions.x >= proportions.y){
		proportions.min = proportions.y;
	}
	else {
		proportions.min = proportions.x;
	}

	pauseButton = {
		button: 0,
		click: false,
		bg: 0,
		statusText: 0,
		homeButton: 0,
		helpButton: 0,
		exitButton: 0
	};

	helpState = {
		bg: 0,
		exitButton: 0,
		helpText: 0
	};

	gameState = {
		actual: "GAME",
		next: 0
	};
	changeState = false;

    nextButton.button = game.add.sprite(game.width * 0.8, game.height * 0.9, 'next');
    nextButton.button.anchor.setTo(0.5, 0.5);
    nextButton.button.scale.setTo(proportions.min*0.4, proportions.min*0.4);
    nextButton.button.inputEnabled = true;
    nextButton.button.events.onInputDown.add(this.nextTutorial, this);

    nextButton.text = this.add.text( game.width*0.8 , game.height*0.6, '',
		{ font: '20px Trebuchet MS', fill: '#fff', align: 'center' });
    nextButton.text.anchor.setTo(0.5, 0.5);
    nextButton.text.scale.setTo(proportions.min, proportions.min);
    nextButton.text.setText("¡Bienvenido al Memorium!\nPresiona el botón de abajo\npara continuar.\n"+
        nextButton.currentState+"/"+nextButton.totalState);

	nextButton.image = game.add.sprite(game.width*0.8, game.height*0.2, '');
	nextButton.image.anchor.setTo(0.5, 0.5);

	//	BOTON DE PAUSA
	pauseButton.button = game.add.sprite(game.width, 0, 'pause');
	pauseButton.button.anchor.setTo(1, 0);
	pauseButton.button.scale.setTo(0.1*proportions.min, 0.1*proportions.min);
	pauseButton.button.inputEnabled = true;
	pauseButton.button.events.onInputDown.add(this.changeState, this);

	pauseButton.bg = game.add.sprite(game.width*0.5, game.height*0.5, 'button');
	pauseButton.bg.anchor.setTo(0.5, 0.5);
	pauseButton.bg.scale.setTo(proportions.min, proportions.min);
	pauseButton.bg.visible = false;

	// pauseButton.statusText = this.add.text( game.width*0.5 , game.height*0.5 - 50*proportions.min, '',
	// 	{ font: '20px Trebuchet MS', fill: '#fff', align: 'center' });
	// pauseButton.statusText.anchor.setTo(0.5, 0.5);
	// pauseButton.statusText.scale.setTo(proportions.min, proportions.min);
	// pauseButton.statusText.visible = false;

	pauseButton.homeButton = game.add.sprite(game.width*0.5 , game.height*0.5, 'home');
	pauseButton.homeButton.anchor.setTo(0.5, 0.5);
	pauseButton.homeButton.scale.setTo(0.25*proportions.min, 0.25*proportions.min);
	pauseButton.homeButton.inputEnabled = true;
	pauseButton.homeButton.events.onInputDown.add(this.homeScreen, this);
	pauseButton.homeButton.visible = false;

	pauseButton.exitButton = game.add.sprite(game.width*0.5 + 100*proportions.min, game.height*0.5, 'wrong');
	pauseButton.exitButton.anchor.setTo(0.5, 0.5);
	pauseButton.exitButton.scale.setTo(0.15*proportions.min, 0.15*proportions.min);
	pauseButton.exitButton.events.onInputDown.add(this.changeState, this);
	pauseButton.exitButton.visible = false;
	pauseButton.exitButton.use = "exit";

	pauseButton.helpButton = game.add.sprite(game.width*0.5 - 100*proportions.min, game.height*0.5, 'help');
	pauseButton.helpButton.anchor.setTo(0.5, 0.5);
	pauseButton.helpButton.scale.setTo(0.25*proportions.min, 0.25*proportions.min);
	pauseButton.helpButton.events.onInputDown.add(this.changeState, this);
	pauseButton.helpButton.visible = false;
	pauseButton.helpButton.use = "help";

	helpState.bg = game.add.sprite(game.width*0.5, game.height*0.5, 'button');
	helpState.bg.anchor.setTo(0.5, 0.5);
	helpState.bg.scale.setTo(1.5*proportions.min, 3*proportions.min);
	helpState.bg.visible = false;

	helpState.exitButton = game.add.sprite(game.width*0.5, game.height*0.2, 'wrong');
	helpState.exitButton.anchor.setTo(0.5, 0.5);
	helpState.exitButton.scale.setTo(0.15*proportions.min, 0.15*proportions.min);
	helpState.exitButton.events.onInputDown.add(this.changeState, this);
	helpState.exitButton.visible = false;

	helpState.relations = game.add.sprite(game.width*0.5, game.height*0.5, 'relation');
	helpState.relations.scale.setTo(proportions.min, proportions.min);
	helpState.relations.anchor.setTo(0.5, 0.5);
	helpState.relations.visible = false;
	helpState.relations.tint = "0x000000";

	cardGroup.alpha =0.3;

	// helpState.helpText = this.add.text( game.width*0.5 , game.height*0.5, '',
	// 	{ font: '20px Trebuchet MS', fill: '#fff', align: 'center' });
	// helpState.helpText.anchor.setTo(0.5, 0.5);
	// helpState.helpText.scale.setTo(proportions.min, proportions.min);
	// helpState.helpText.visible = false;
	// helpState.helpText.setText("La primera parte del juego consite en: \n" +
	// 	"selecciona la expresion que relacione el angulo y los lados\n" +
	// 	" en la pregunta. \n" +
	// 	"Si la respuesta es correcta, entonces se pasa a la siguiente etapa y \n" +
	// 	"una de las estrellas empezara a girar. Pasaras de nivel cuando todas \n" +
	// 	"las estrellas giren. \n " +
	// 	"Falla dos preguntas y tendras que resolver algo para seguir intentando.");

	// statusText = game.add.bitmapText(game.width*0.8, game.height*0.1, "retroFont", "", 20);
	// // statusText.text.align = "center";
	// statusText.align = "center";
	// statusText.alpha = 0;
	// statusText.anchor.setTo(0.5, 0.5);
	// statusText.setText('Nivel: '+game.level+'\nEtapa: '+stage+'\nFallos: '+fails);
	// statusText.scale.setTo(proportions.x, proportions.y);

};


Formularium.Memorium_tutorial.prototype.update = function ()
{
	if (changeState){
		changeState = false;

		if (gameState.actual == "GAME"){

			gameState.actual = "PAUSE";
			pauseButton.bg.visible = true;
			pauseButton.bg.bringToTop();
			// pauseButton.statusText.visible = true;
			// pauseButton.statusText.setText('Level> '+game.level+' Stage> '+stage+' Fails> '+fails);
			// pauseButton.statusText.bringToTop();
			pauseButton.homeButton.visible = true;
			pauseButton.homeButton.bringToTop();
			pauseButton.exitButton.visible = true;
			pauseButton.exitButton.bringToTop();
			pauseButton.exitButton.inputEnabled = true;
			pauseButton.helpButton.visible = true;
			pauseButton.helpButton.bringToTop();
			pauseButton.helpButton.inputEnabled = true;

		}
		else if (gameState.actual == "PAUSE"){

			gameState.actual = "GAME";
			pauseButton.bg.visible = false;
			// pauseButton.statusText.visible = false;
			pauseButton.homeButton.visible = false;
			pauseButton.exitButton.visible = false;
			pauseButton.helpButton.visible = false;


			if(gameState.next == "HELP"){

				gameState.actual = "HELP";
				helpState.bg.visible = true;
				helpState.bg.bringToTop();
				helpState.exitButton.visible = true;
				helpState.exitButton.inputEnabled = true;
				helpState.exitButton.bringToTop();
				// helpState.helpText.visible = true;
				// helpState.helpText.bringToTop();
				helpState.relations.visible = true;
				helpState.relations.bringToTop();
			}
			else {
				pauseButton.button.inputEnabled = true;
			}
		}
		else if (gameState.actual == "HELP"){

			gameState.actual = "GAME";
			helpState.bg.visible = false;
			helpState.exitButton.visible = false;
			// helpState.helpText.visible = false;
			helpState.relations.visible = false;
			pauseButton.button.inputEnabled = true;
		}

		console.log("GameState: "+gameState.actual);
	}
	// //Acomodar aqui para el timer
	// var timeLimit = 5;
	// switch(Formularium.level){
	//
	// 	case 'easy':
	// 		timeLimit = 10;
	// 		mySeconds = this.game.time.totalElapsedSeconds();
	// 		console.log(mySeconds);
	// 		if (mySeconds > timeLimit)				{
	// 				// time is up
	// 				this.game.time.reset();
	// 				this.state.start('MemoriumOver');
	//
	// 		}
	// 	break;
	//
	// 	case 'medium':
	// 		timeLimit = 18;
	// 		mySeconds = this.game.time.totalElapsedSeconds();
	// 		console.log(mySeconds);
	// 		if (mySeconds > timeLimit)				{
	// 				// time is up
	// 				this.game.time.reset();
	// 				this.state.start('MemoriumOver');
	//
	// 		}
	//
	// 	break;
	//
	// 	case 'hard':
	// 		timeLimit = 25;
	// 		mySeconds = this.game.time.totalElapsedSeconds();
	// 		console.log(mySeconds);
	// 		if (mySeconds > timeLimit)				{
	// 				// time is up
	// 				this.game.time.reset();
	// 				this.state.start('MemoriumOver');
	//
	// 		}
	//
	// 	break;
	//
	// }
	// myCountdownSeconds = timeLimit - mySeconds;
	// console.log(myCountdownSeconds);
	// if (myCountdownSeconds <= 0)				{
	// 		// time is up
	// 		myCountdownSeconds = 0;
	// 		this.game.time.reset();
	// 		//console.log(myCountdownSeconds);
	// 		this.state.start('MemoriumOver');
	//
	// }

};

// Funcion para el timer
// timer = function(timeLimit, mySeconds){
// 	//  Create our Timer
// 	// var timer = this.game.time.create(false);
// 	// //  Set a TimerEvent to occur after "timeLimit" seconds
//   // // timer.loop(timeLimit, this.state.start('MemoriumOver'), this);
// 	// //  Start the timer running - this is important!
// 	// //  It won't start automatically, allowing you to hook it to button events and the like.
// 	// timer.start();
// 	// console.log(mySeconds);
// 	// if (mySeconds > timeLimit)				{
// 	// 		// time is up
// 	// 		this.game.time.reset();
// 	// 		this.state.start('MemoriumOver');
// 	//
// 	// }
// },

	//Funcion que crea el tablero y coloca las catas de aceurdo al nivel seleccionado
Formularium.Memorium_tutorial.prototype.buildCards = function(h,w,hs,vs,ox,oy,sc, tL)
{
	flippedCards = [];
	facingCards = [];
	cards = h*w;
	//create array of matched pairs
	for(var c = 0; c<w*h/2;c++){
		var aux = c + (w*h/2);
		cardList.push(c);
		cardList.push(aux);
	}
	for(var i=0; i< w;i++)
	{

		for(var t=0;t<h;t++)
		{
			//show card back and randomise
			var card = this.game.add.sprite(i*hs+ox,0 , 'cardBack');
			var r = Math.floor(Math.random()*(cardList.length));

			card.value = cardList[r];

			cardList.splice(r,1);

			card.anchor.setTo(0.5,0.5);

			card.inputEnabled = true;

			card.flipped = false;

			card.events.onInputDown.add(this.flip, this);

			cardGroup.add(card);

			tween = this.game.add.tween(card).to( {y: t*vs+oy }, 500 + (i*200.5), Phaser.Easing.Bounce.Out, false);

			tween.start();
		}

	}

};

Formularium.Memorium_tutorial.prototype.flipBack = function()
{

	inPlay = [];
	this.flipper(facingCards[0]);
	this.flipper(facingCards[1]);


};

Formularium.Memorium_tutorial.prototype.flipper = function(targ)
{

	var tween = this.game.add.tween(targ.scale).to({ x: 1.5, y: 1.5}, 100, Phaser.Easing.Linear.None);

		tween.onComplete.add(function(){
			this.hideCard(targ);
					}, this);
		tween.start();

};

Formularium.Memorium_tutorial.prototype.hideCard = function(targ)
{

	var tween = this.game.add.tween(targ.scale).to({ x: 0}, 100, Phaser.Easing.Linear.None);
	tween.onComplete.add(function(){
			flippedCards[0].visible = true;
			flippedCards[1].visible = true;
			this.hider(flippedCards[0]);
			this.hider(flippedCards[1]);

				}, this);
	    tween.start();
};

Formularium.Memorium_tutorial.prototype.hider = function(targ)
{
	var tween = this.game.add.tween(targ.scale).to({ x: 1.5, y: 1.5}, 100, Phaser.Easing.Linear.None);


		tween.onComplete.add(function(){

				targ.flipped = false;
				targ.inputEnabled = true;
				firstCardClicked =  false;
				secondCardClicked = false;

				this.scaleCard(targ);
					}, this);
		tween.start();
};

//Funcion para comparar las cartas seleccionadas
Formularium.Memorium_tutorial.prototype.flip = function(targ)
{

	//var aux = boardWidth*boardHeight/2;

	if(secondCardClicked == false){
		if(targ.flipped == false ){
			if(firstCardClicked == false){
				firstCardClicked = true;

				//Si el numero es par o 0 le dejo el valor, de lo contrario coloco el anterior (que sera par o 0)
				if (targ.value%2 == 0){
					firstCardValue = targ.value;
				} else {
					firstCardValue = targ.value - 1;
				}

				// firstCardValue = targ.value;
				inPlay[0] = targ;
			}
			else if(firstCardClicked == true && secondCardClicked == false){
				secondCardClicked = true;

				//Si el numero es par o 0 le dejo el valor, de lo contrario coloco el anterior (que sera par o 0)
				if (targ.value%2 == 0){
					secondCardValue = targ.value;
				} else {
					secondCardValue = targ.value - 1;
					//if(firstCardValue == 0 && secondCardValue == aux || firstCardValue == aux && secondCardValue == 0){
					//	secondCardValue = firstCardValue;
					//}
				}

				// secondCardValue = targ.value;
				if(firstCardValue != secondCardValue){
					this.game.time.events.add(Phaser.Timer.SECOND * 1, this.flipBack, this);
				}
				else if(firstCardValue == secondCardValue){
					inPlay[1] = targ;
					//YO (MANUEL) COMENTE ESTA LINEA
					//console.log(inPlay.length);
					cards -=2;
					this.correct();

	   		 	flippedCards = [];
	   			facingCards = [];

					firstCardClicked =  false;
					secondCardClicked = false;

				}

			}

			var tween = this.game.add.tween(targ.scale).to({ x: 1.5, y: 1.5}, 100, Phaser.Easing.Linear.None);
		    //cardFlip.play();
			targ.flipped = true;
			targ.inputEnabled = false;
			tween.onComplete.add(function(){
					this.showCard(targ);
						}, this);
			tween.start();
			if(firstCardClicked == true && secondCardClicked == false){
				flippedCards[0]=targ;
			}
			else if(firstCardClicked == true && secondCardClicked == true){
				flippedCards[1]=targ;
			}
			//YO (MANUEL) COMENTE ESTA LINEA
			//console.log(targ.value);
		}
	}
};

// Funcion que llama "showFace"
Formularium.Memorium_tutorial.prototype.showCard = function(targ)
{

	var tween = this.game.add.tween(targ.scale).to({ x: 0}, 100, Phaser.Easing.Linear.None);
	tween.onComplete.add(function(){
			this.showFace(targ);
				}, this);
	    tween.start();

};

//
Formularium.Memorium_tutorial.prototype.correct = function(i)
{

	//correct.play();

	emitter.start(true, 1000, null, 20);
	emitter.x = inPlay[0].x;
	emitter.y = inPlay[0].y;
	emitter2.start(true, 1000, null, 20);
	emitter2.x = inPlay[1].x;
	emitter2.y = inPlay[1].y;
	if(cards ===0){

		cardGroup.alpha =0.5;
		/*textGroup = this.game.add.group();

		var gameComplete = this.game.add.sprite(this.game.world.centerX, -100, 'gameComplete');
		gameComplete.anchor.setTo(0.5,0.5);
		textGroup.z = 0;
		textGroup.add(gameComplete);

		var tween = this.game.add.tween(gameComplete).to({ y:200}, 1500, Phaser.Easing.Bounce.Out);
		tween.onComplete.add(function(){
			gameComplete.inputEnabled = true;
			//gameComplete.events.onInputDown.add(this.reloadMemorium, this);
			gameComplete.events.onInputDown.add(this.quitMemorium, this);
			//fanfair.play();
			//roar.play();

					}, this);
		tween.start();


		var well = this.game.add.sprite(1400,600,'welldone');
		well.anchor.setTo(0.5,0.5);

		var tween2 = this.game.add.tween(well).to({ x:800}, 600, Phaser.Easing.Linear.None);
		tween2.onComplete.add(function(){
			well.inputEnabled = true;
			// Cambiar "this.quitMemorium" por el estado para ir a Constelaciones
			well.events.onInputDown.add(this.quitMemorium, this);
			//roar.play();

					}, this);
		tween2.start();*/
		this.resultState();



	}
};

Formularium.Memorium_tutorial.prototype.resultState = function () {
    let resultUI = {
        bg: 0,
        star: []
    };
    fails = 0;
    blackStars = fails;
    if (fails >= 1) {
        blackStars = 1;
    }
    if (fails >= 3) {
        blackStars = 2;
    }
    if (fails >= 6) {
        blackStars = 3;
    }

    resultUI.bg = game.add.sprite(game.width * 0.5, game.height *0.5, 'button');
    resultUI.bg.scale.setTo(proportions.min, 2*proportions.min);
    resultUI.bg.anchor.setTo(0.5);

    nextLvlButton = this.add.button(game.width*0.5, game.height*0.6, 'next', this.quitMemorium, this);
    nextLvlButton.anchor.setTo(0.5, 0.5);
    nextLvlButton.scale.setTo(0.5*proportions.min);

    for (let i = 0; i < 3; i++) {
        resultUI.star[i] = game.add.sprite(game.width * 0.1 * (4 + i), game.height * (0.5 - ((i%2) * 0.1)), 'starScore');
        resultUI.star[i].scale.setTo(proportions.min*0.5);
        resultUI.star[i].anchor.setTo(0.5);
        if (3 - blackStars <= i) {
            resultUI.star[i].tint = "0x666666";
        }
    }
},

// Funcion que meustra las cartas volteadas de acuerdo a su valor
Formularium.Memorium_tutorial.prototype.showFace = function(targ)
{

	var val = targ.value+1;
	var card;
	var card = this.game.add.sprite(targ.x, targ.y, 'card'+val);

	card.scale.setTo(0,1.5);
	card.anchor.setTo(0.5,0.5);

	card.value = targ.value;

	cardGroup.add(card);

	var tween = this.game.add.tween(card.scale).to({ x: 1.5}, 100, Phaser.Easing.Linear.None);
	tween.onComplete.add(function(){
			this.scaleCard(card);
				}, this);
	tween.start();
	if(firstCardClicked == true && secondCardClicked == false){
		facingCards[0]= card;
	}
	else if(firstCardClicked == true && secondCardClicked == true){
		facingCards[1]= card;
	}

};

Formularium.Memorium_tutorial.prototype.scaleCard = function(targ)
{
	var tween = this.game.add.tween(targ.scale).to({ x: 1,y:1}, 100, Phaser.Easing.Linear.None);
	tween.onComplete.add(function(){

				}, this);
	tween.start();
};

//Funcion que termina Memorium e inicia Constelaciones
Formularium.Memorium_tutorial.prototype.quitMemorium = function ()
{

	//Agregar aqui el nombre del archivo de Constelaciones
	//this.state.start('MainMenu');
	var estrellas = 3;
	if(game.global.starsArray[game.global.level-1]<estrellas)
	{
		game.global.starsArray[game.global.level-1] = estrellas;
		firebase.database().ref("/users/" + this.game.usuario.value + "/starsArray").set(game.global.starsArray);
	}
	let change = false;
	// if we completed a level and next level is locked - and exists - then unlock it
	if(estrellas>0 && game.global.starsArray[game.global.level]==4 && game.global.level<game.global.starsArray.length)
	{
		game.global.starsArray[game.global.level] = 0;
		change = true;
	}
	if (estrellas>0 && game.global.starsArray[game.global.level+3]==4 && game.global.level<game.global.starsArray.length-4)
	{
		game.global.starsArray[game.global.level+3] = 0;
		change = true;
	}
	if (change == true)
	{
		change = false;
		firebase.database().ref("/users/" + this.game.usuario.value + "/starsArray").set(game.global.starsArray);
	}
	this.game.state.start("BootState", true, false, "assets/levels/level_select.json", "LevelSelect");
};

// Funcion que vuelve a cargar Memorium
Formularium.Memorium_tutorial.prototype.reloadMemorium = function ()
{

	this.state.start('MainMenu');
	//this.game.state.start("BootState", true, false, "assets/levels/level_select.json", "LevelSelect");
};

Formularium.Memorium_tutorial.prototype.homeScreen = function ()
{
	//YO MANUEL COMENTE ESTA LINEA
	//this.state.start('Menu');
	this.game.state.start("BootState", true, false, "assets/levels/level_select.json", "LevelSelect");
},

Formularium.Memorium_tutorial.prototype.changeState = function (buton)
{
	changeState = true;
	buton.inputEnabled = false;
	if (buton.use == "help"){
		console.log(buton.use);
		gameState.next = "HELP";
	}
	else if (buton.use == "exit"){
		console.log(buton.use);
		gameState.next = "GAME";
	}
}

Formularium.Memorium_tutorial.prototype.nextTutorial = function () {
    if (nextButton.currentState < nextButton.totalState) {
        nextButton.currentState++;
        nextButton.text.setText(tutorial[nextButton.currentState]+"\n"+
            nextButton.currentState+" \\ "+nextButton.totalState);
        // if (nextButton.currentState == 1) {
        // 	nextButton.image.loadTexture('rectTri');
        // }
        if (nextButton.currentState == 1) {
        	//nextButton.image = game.add.sprite(game.width*0.5, game.height*0.5, 'relation');
        	nextButton.image.loadTexture('relationW');
        	nextButton.image.scale.setTo(proportions.min*0.6, proportions.min*0.6);
        	//nextButton.image.tint = "0xff0000";
        }
    } else {
        nextButton.text.visible = false;
        nextButton.button.visible = false;
        nextButton.image.visible = false;
		cardGroup.alpha = 1;

        // statusText.visible = true;
        // question.expresion.visible = true;
        // question.enunciado.visible = true;
        // readyButton.button.visible = true;
        // readyButton.text.visible = true;
        // for (var i = 0, n = options.length; i < n; i++) {
        //     //options[i].button.visible = true;
        //     //options[i].text.visible = true;
        //     questOptions[i].button.visible = true;
        //     questOptions[i].text.visible = true;
        // }
    }
}
