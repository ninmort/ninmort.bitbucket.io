var Formularium = Formularium || {};

Formularium.Identificacion = function () {
    "use strict";
    Phaser.State.call(this);
};

Formularium.Identificacion.prototype = Object.create(Phaser.State.prototype);
Formularium.Identificacion.prototype.constructor = Formularium.Identificacion;

Formularium.Identificacion.prototype.init = function ()
{

    this.game.scale.setGameSize(1270, 740);
    this.game.scale.pageAlignHorizontally = true;
	this.game.scale.pageAlignVertically = true;
	this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
	this.game.scale.updateLayout();
}

Formularium.Identificacion.prototype.create = function ()
{
	// game.background = game.add.tileSprite(0, 0, game.world.width, game.world.height, 'spaceBG');
	// game.background.autoScroll(12, 24);
	game.level = game.global.level;
	game.stage.backgroundColor = '#061f27';
	gameBG = game.add.tileSprite(0, 0, game.width, game.height, 'spaceBG');
	gameBG.autoScroll(game.rnd.integerInRange(-5, 5), game.rnd.integerInRange(-5, 5));
	gameBG.alpha = 0.01*game.rnd.integerInRange(20,50);
	//	INICIALIZACION
	animation = {
		option: false,
		introText: true,
	};
	onCoord = false;		//	booleano para mostrar coordenadas
	star = [];				//	contiene las estrellas
	triangle = [[]];		//	contiene los triangulos
	nTriangles = 1;			//	cantidad de triangulos
	questTriangle = 0;		//	triangulo involucrado con la pregunta
	coord = [];
	options = [];			//	contiene los botones
	questionsPool = [];		//	contiene las preguntas
	choosenQuestions = [];	//	secuencia de preguntas
	iStars = 3;				//	numero inicial de estrellas
	finalStars = 0;			//	numero final de estrellas
	winLevel = false;		//	indicador de nivel ganado
	nOptions = 3;			//	numero de opciones
	fails = 0;				//	cuenta la cantidad de fallos
	addStars = false;		//	agrega nueva estrellas
	var blackStars = 0;
	proportions = {
		x: game.width/1270,
		y: game.height/740,
		min: 0
	};
	if(proportions.x >= proportions.y){
		proportions.min = proportions.y;
	}
	else {
		proportions.min = proportions.x;
	}

	introText = 0;

	nStages = 0;			//	numero de etapas
	stage = 1;				//	etapa actual
	newQuestion = true;	//	nueva pregunta para la nueva etapa

	rotateStars = {
		en: true,			//	habilita las estrellas girando
		theseStars: [],
		num: 0				//	cuantas estrellas giran que es igual a la etapa actual
	};
	lines = [];
    lineTween = [];

	pauseButton = {
		button: 0,
		click: false,
		bg: 0,
		statusText: 0,
		homeButton: 0,
		helpButton: 0,
		exitButton: 0
	};

	helpState = {
		bg: 0,
		exitButton: 0,
		helpText: 0,
		relations: 0
	};

	gameState = {
		actual: "GAME",
		next: 0
	};
	changeState = false;

	alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'];	//	nombre de las estrellas
	answers = ['seno', 'coseno', 'tangente'];
	color = [0xff0000, 0x00ff00, 0x00ffff];



	//	NIVEL
    if((game.global.level/4) <= 2){
		finalStars = 3;
	} else if((game.global.level/4) <= 7){
		finalStars = 6;
	} else {
		finalStars = 9;
	}

	//	INICIALIZACION DE ESTRELLAS (UBICACION)
	spotsX = [];	//	almacena posiciones en X
	spotsY = [];	//	almacena posiciones en Y

    pistas = {
        text: this.add.text(game.width*0.8, game.height, '',
			{ font: '20px Trebuchet MS', fill: '#fff', align: 'center' }),
        clue: []
    };
    pistas.text.anchor.setTo(0.5, 1);

	nStages = finalStars + 1 - (finalStars/3);		//	numero de etapas es igual al numero de estrellas

	hold = this.shuffle(this.createOrderArray(1, 10));
	spotsX = hold.slice(0, nStages);
	hold = this.shuffle(this.createOrderArray(1, 10));
	spotsY = hold.slice(0, nStages);
	spotsY = spotsY.sort();

	//	CONFIGURACION PARA AÑADIR LA TERCERA ESTRELLA
	if ((spotsX[0]>((spotsX[0]+spotsX[1])/2)) && (spotsY[0]>((spotsY[0]+spotsY[1])/2))
		||(spotsX[1]>((spotsX[0]+spotsX[1])/2)) && (spotsY[1]>((spotsY[0]+spotsY[1])/2))){
		hold = spotsX.slice(0,2);						//	obtiene los dos elementos del array
		hold.sort();									//
		spotsX[hold.length] = hold[hold.length-1];
		hold = spotsY.slice(0,2);						//	obtiene los dos elementos del array
		hold.sort();									//
		spotsY[hold.length] = hold[0];
	} else {
		hold = spotsY.slice(0,2);						//	obtiene los dos elementos del array
		hold.sort();									//
		spotsY[hold.length] = hold[0];
		hold = spotsX.slice(0,2);						//	obtiene los dos elementos del array
		hold.sort();									//
		spotsX[hold.length] = hold[0];
	}

	triangle[0] = [];		//	inicializa el primer triangulo
	// hold = this.shuffle(this.createOrderArray(0, 3));	//	para que sea aleatorio el nombre de las estrellas

	for(var i = 0, j = 0, k = 0; i<finalStars; i++){				//	genera las estrellas * PENDIENTE DEL spotsX.length

		star[i] = {
			x: 0,						//	guarda las coordenadas en X
			y: 0,						//	guarda las coordenadas en Y
			obj: 1,								//	la estrella como asset
			text: 1,							//	el texto que se muestra como leyenda y coordenada
			id: 0,		//	le agrega un nombre
			adj: [],
			op: [],
            emitter: 0
		};

		if ((i == 0)||((i%3)!=0)){

			if(((i%3)!=2)||(i==2)){
				star[i].x = spotsX[k];
				star[i].y = spotsY[k];
				k++;
				console.log("posicion a: "+i);
			}
			star[i].id = alphabet[j];
			rotateStars.theseStars[j] = i;
			j++;
			console.log("letra a :" + i);
		}
		// console.log("i: "+i+" j: "+j+" k: "+k);

		star[i].obj = game.add.sprite(game.height*0.1*star[i].x, game.height*0.1*(10-star[i].y), 'star');
		star[i].obj.anchor.setTo(0.5, 0.5);
		star[i].obj.scale.setTo(0.5*proportions.min, 0.5*proportions.min);
		star[i].text = this.add.text(star[i].obj.x + 15*proportions.x, star[i].obj.y + 15*proportions.y, '',
			{ font: '20px Trebuchet MS', fill: '#fff', align: 'center' }
			);
		//star[i].text = game.add.bitmapText(star[i].obj.x + 15*proportions.min, star[i].obj.y + 15*proportions.min, "retroFont", "", 20);
		star[i].text = this.add.text(star[i].obj.x + 15*proportions.min, star[i].obj.y + 15*proportions.min, '',
			{ font: '20px Trebuchet MS', fill: '#fff', align: 'center' });
		star[i].text.anchor.setTo(0.5, 0.5);
		star[i].text.scale.setTo(proportions.min, proportions.min);
		star[i].text.visible = false;

		if(i >= iStars){
			// star[i].obj.visible = false;
			// star[i].text.visible = false;
		}
		else{
			triangle[0][i] = i;
			// star[i].id = alphabet[hold[i]];
		}

		star[i].text.setText(star[i].id);
	};

	console.log("star length: "+star.length);

	// console.log("length: "+triangle[0].length);

	for(var i = 0; i < triangle[questTriangle].length; i++){			//	asigna cateto opuesto y adjacente

		if (i == (iStars-1)){
			star[i].adj = [triangle[questTriangle][0], triangle[questTriangle][1]];
			star[i].op = star[i].adj;
		}
		else {
			star[i].adj = [triangle[0][i], triangle[0][triangle[0].length-1]];
			hold = triangle[0].slice(0);				//	duplica el array triangle
			hold.splice(i,1);						//	elimina el cateto adjacente del array triangle
			star[i].op = [hold[0], triangle[0][triangle[0].length-1]];
			// console.log(i+" adj: "+star[i].adj+" op: "+star[i].op);
		}
	};

	// introText = this.add.text(game.width*0.5, game.height*0.5, '',
	// 	{ font: '20px Trebuchet MS', fill: '#fff', align: 'center' });
	// introText = game.add.bitmapText(game.width*0.5, game.height*0.5, "retroFont", "Nivel: "+game.level, 35);
	introText = this.add.text( game.width*0.5 , game.height*0.5, '',
		{ font: '20px Trebuchet MS', fill: '#fff', align: 'center' });
	introText.anchor.setTo(0.5, 0.5);
	introText.scale.setTo(4*proportions.min, 4*proportions.min);
	// introText.align = "center";

	//	BOTON DE PAUSA
	pauseButton.button = game.add.sprite(game.width, 0, 'pause');
	pauseButton.button.anchor.setTo(1, 0);
	pauseButton.button.scale.setTo(0.1*proportions.min, 0.1*proportions.min);
	pauseButton.button.inputEnabled = true;
	pauseButton.button.events.onInputDown.add(this.changeState, this);

	pauseButton.bg = game.add.sprite(game.width*0.5, game.height*0.5, 'button');
	pauseButton.bg.anchor.setTo(0.5, 0.5);
	pauseButton.bg.scale.setTo(proportions.min, proportions.min);
	pauseButton.bg.visible = false;

	pauseButton.homeButton = game.add.sprite(game.width*0.5 , game.height*0.5, 'home');
	pauseButton.homeButton.anchor.setTo(0.5, 0.5);
	pauseButton.homeButton.scale.setTo(0.25*proportions.min, 0.25*proportions.min);
	pauseButton.homeButton.inputEnabled = true;
	pauseButton.homeButton.events.onInputDown.add(this.homeScreen, this);
	pauseButton.homeButton.visible = false;

	pauseButton.exitButton = game.add.sprite(game.width*0.5 + 100*proportions.min, game.height*0.5, 'wrong');
	pauseButton.exitButton.anchor.setTo(0.5, 0.5);
	pauseButton.exitButton.scale.setTo(0.15*proportions.min, 0.15*proportions.min);
	pauseButton.exitButton.events.onInputDown.add(this.changeState, this);
	pauseButton.exitButton.visible = false;
	pauseButton.exitButton.use = "exit";

	pauseButton.helpButton = game.add.sprite(game.width*0.5 - 100*proportions.min, game.height*0.5, 'help');
	pauseButton.helpButton.anchor.setTo(0.5, 0.5);
	pauseButton.helpButton.scale.setTo(0.25*proportions.min, 0.25*proportions.min);
	pauseButton.helpButton.events.onInputDown.add(this.changeState, this);
	pauseButton.helpButton.visible = false;
	pauseButton.helpButton.use = "help";

	helpState.bg = game.add.sprite(game.width*0.5, game.height*0.5, 'button');
	helpState.bg.anchor.setTo(0.5, 0.5);
	helpState.bg.scale.setTo(1.5*proportions.min, 3*proportions.min);
	helpState.bg.visible = false;

	helpState.exitButton = game.add.sprite(game.width*0.5, game.height*0.2, 'wrong');
	helpState.exitButton.anchor.setTo(0.5, 0.5);
	helpState.exitButton.scale.setTo(0.15*proportions.min, 0.15*proportions.min);
	helpState.exitButton.events.onInputDown.add(this.changeState, this);
	helpState.exitButton.visible = false;

	helpState.relations = game.add.sprite(game.width*0.5, game.height*0.5, 'relation');
	helpState.relations.scale.setTo(proportions.min, proportions.min);
	helpState.relations.anchor.setTo(0.5, 0.5);
	helpState.relations.visible = false;
	helpState.relations.tint = "0x000000";

	statusText = this.add.text( game.width*0.8 , game.height*0.05, '',
		{ font: '20px Trebuchet MS', fill: '#fff', align: 'center' });
	statusText.anchor.setTo(0.5, 0);
	statusText.setText('Nivel: '+game.level+' Etapa: '+stage+'/'+ nStages + ' Fallos: '+fails);
	statusText.scale.setTo(proportions.x, proportions.y);

	//	PREGUNTAS

	// choosenQuestions = this.notRepeatRandom(((iStars-1)*nOptions)-1,iStars);	//	se escogen las preguntas del nivel aleatorio
	//choosenQuestions = this.shuffle(this.createOrderArray(0, (iStars-1)*(finalStars/3)*nOptions));
	choosenQuestions = this.shuffle(this.createOrderArray(0, 12*(finalStars/3)));
	console.log("choosenQuestion> "+choosenQuestions);

	question = {		//	formato
		num: 0,
		text: this.add.text(
		game.width*0.8, game.height*0.2,
		'', { font: '20px Trebuchet MS', fill: '#fff', align: 'center' }),
	};
	question.text.anchor.setTo(0.5, 0.5);
	question.text.scale.setTo(proportions.min, proportions.min);
	// question.text.strokeThickness = 1;

	//	lista de preguntas
	questionsPool[0] = {
		type: ' ',
	};

	for (var i = 0; i < nOptions; i++){		//	genera las opciones

		options[i] = {
			x: game.width*0.8,		//	EDITAR: no tiene sentido
			y: game.height*0.4 + 100*i*proportions.min,
			checkImage: 0,
			button: 0,
			text: 0,
		};

		options[i].button = game.add.sprite(options[i].x*4, options[i].y, 'button');
		options[i].button.anchor.setTo(0.5, 0.5);
		options[i].button.scale.setTo(0.5*proportions.min, 0.5*proportions.min);
		options[i].button.inputEnabled = true;
		options[i].button.number = i;
        options[i].button.tint = "0x446600";

		options[i].text = this.add.text(options[i].x*4, options[i].y, '',
			{ font: '20px Trebuchet MS', fill: '#fff', align: 'center' }
			);
		options[i].text.anchor.setTo(0.5, 0.5);
		options[i].text.setText(answers[i]);
		options[i].text.scale.setTo(proportions.min, proportions.min);
		options[i].checkImage = game.add.sprite(200, 200, '');	//	QUITAR

	};

 	for (var i = 0; i < iStars ; i++){		//	ultimo render sea la estrella
 		star[triangle[questTriangle][i]].obj.bringToTop();
 		star[triangle[questTriangle][i]].obj.alpha = 0;
 		game.add.tween(star[triangle[questTriangle][i]].obj).to({alpha: 1}, 1000, 'Linear', true);
 		hold = game.rnd.integerInRange(0, 30);
 		game.add.tween(star[triangle[questTriangle][i]].obj.scale).to({x: 0.5 + hold*0.01, y: 0.5 + hold*0.01}, 1000 + game.rnd.integerInRange(0, 500), 'Linear', true, game.rnd.integerInRange(0, 200), -1, true);
 	}


 	if(finalStars/3 > (questTriangle+1)){
 		addStars = true;
 	}

	while(addStars){

		questTriangle++;
		// addStars = false;
		triangle[questTriangle] = [];

		// hold = this.shuffle(this.createOrderArray(0, 3));	//	aleatorio entre las estrellas del triangulo previo
		hold = [];
		if (star[triangle[questTriangle-1][0]].y > star[triangle[questTriangle-1][1]].y){
			hold[0] = 0;
		} else {
			hold[0] = 1;
		}
		// console.log("nombre star repeat: "+star[triangle[questTriangle-1][hold]].id);
		triangle[questTriangle][0] = questTriangle*3;
		star[triangle[questTriangle][0]].id = star[triangle[questTriangle-1][hold[0]]].id;
        star[triangle[questTriangle][0]].text.setText(star[triangle[questTriangle-1][hold[0]]].id);
		star[triangle[questTriangle][0]].x = star[triangle[questTriangle-1][hold[0]]].x;
		star[triangle[questTriangle][0]].y = star[triangle[questTriangle-1][hold[0]]].y;
		star[triangle[questTriangle][0]].obj.x = star[triangle[questTriangle-1][hold[0]]].obj.x;
		star[triangle[questTriangle][0]].obj.y = star[triangle[questTriangle-1][hold[0]]].obj.y;

		triangle[questTriangle][1] = 1 + questTriangle*3;
		//star[triangle[questTriangle][1]].obj.visible = true;
		// star[triangle[questTriangle][1]].text.visible = true;

		triangle[questTriangle][2] = 2 + questTriangle*3;
		star[triangle[questTriangle][2]].x = star[triangle[questTriangle][0]].x;		//	MODIFICAR
		star[triangle[questTriangle][2]].y = star[triangle[questTriangle][1]].y;		//	MODIFICAR
		star[triangle[questTriangle][2]].obj.x = star[triangle[questTriangle][2]].x*game.height*0.1;
		star[triangle[questTriangle][2]].obj.y = (10-star[triangle[questTriangle][2]].y)*game.height*0.1;
		star[triangle[questTriangle][2]].text.x = star[triangle[questTriangle][2]].obj.x + 15*proportions.min;
		star[triangle[questTriangle][2]].text.y = star[triangle[questTriangle][2]].obj.y + 15*proportions.min;
		//star[triangle[questTriangle][2]].obj.visible = true;
		// star[triangle[questTriangle][2]].text.visible = false;

		for(var i = 0; i<triangle[questTriangle].length-1; i++){			//	asigna cateto opuesto y adjacente
			if (i == triangle[questTriangle].length-1){
				star[triangle[questTriangle][i]].adj = [triangle[questTriangle][0], triangle[questTriangle][1]];
				star[triangle[questTriangle][i]].op = [triangle[questTriangle][0], triangle[questTriangle][1]];
			}
			else {
				star[triangle[questTriangle][i]].adj = [triangle[questTriangle][i], triangle[questTriangle][triangle[questTriangle].length-1]];
				hold = triangle[questTriangle].slice(0);				//	duplica el array triangle
				hold.splice(i,1);						//	elimina el cateto adjacente del array triangle
				star[triangle[questTriangle][i]].op = [hold[0], triangle[questTriangle][triangle[questTriangle].length-1]];
			}
			// console.log(triangle[0][i]+" : adj: "+star[triangle[0][i]].adj + " op: " + star[triangle[0][i]].op);
		};

	 	if(finalStars/3 == (questTriangle+1)){
 			addStars = false;
 		}
	}
	questTriangle = 0;

	let sortedPos = {
		x: [],
		y: []
	}

	for (var i = 0, n = star.length; i < n ; i++){		//	ultimo render sea la estrella
	 	star[i].obj.bringToTop();
		if (star[i].x !== 0  && star[i].y !== 0) {
			sortedPos.x.push(star[i].x);
			sortedPos.y.push(star[i].y);
		}
		// star[triangle[questTriangle][i]].text.bringToTop();
	 }

	 sortedPos.x.sort();
	 sortedPos.y.sort();

	 var propStar = (sortedPos.x[sortedPos.x.length-1] - sortedPos.x[0]);
		 if (propStar < (sortedPos.y[sortedPos.y.length-1] - sortedPos.y[0] + 1)) {
			 propStar = (sortedPos.y[sortedPos.y.length-1] - sortedPos.y[0] + 1);
		 }

	 for (var i = 0, n = star.length; i < n; i++) {
		 game.add.tween(star[i].obj).to({x: game.height * ((star[i].x - sortedPos.x[0])/propStar + 0.1)}, 1000, Phaser.Easing.Linear.None, true, 0, 0, false);

			game.add.tween(star[i].obj).to({y: game.height * ((1 - (star[i].y - sortedPos.y[0])/propStar) - 0.1)}, 1000, Phaser.Easing.Linear.None, true, 0, 0, false);
	 }

	// choosenQuestions.shift();		//	elimina la primera pregunta ya hecha, sigue la secuencia
	intro = game.add.tween(introText).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true, 0, 0, false);
	game.add.tween(statusText).to({alpha: 1}, 1000, Phaser.Easing.Linear.None, true, 0, 0, false);

	intro.onComplete.add(function() {
		trianLines = [];
		for (var i = 0, n = triangle.length; i < n; i++) {
			trianLines[i] = game.add.graphics(0, 0);
			trianLines[i].lineStyle(5, 0xff6600, 0.5);
			trianLines[i].moveTo(star[triangle[i][(triangle[i].length-1)]].obj.x,
				star[triangle[i][(triangle[i].length-1)]].obj.y);
			// trianLines[questTriangle].visible = true;

			trianLines[i + finalStars/3] = game.add.graphics(0, 0);
			trianLines[i + finalStars/3].lineStyle(2, 0xffffff, 1);
			trianLines[i + finalStars/3].moveTo(star[triangle[i][(triangle[i].length-1)]].obj.x,
				star[triangle[i][(triangle[i].length-1)]].obj.y);
			trianLines[i + finalStars/3].visible = true;

			for (var j = 0; j < 3; j++) {
				star[triangle[i][j]].text.x = star[triangle[i][j]].obj.x + 30*proportions.min;
				star[triangle[i][j]].text.y = star[triangle[i][j]].obj.y + 30*proportions.min;
				star[triangle[i][j]].text.visible = true;
				star[triangle[i][j]].text.alpha = 0;
				game.add.tween(star[triangle[i][j]].text).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true, 0, 0, false);

				trianLines[i].lineStyle(5, color[j], 0.5);
				trianLines[i].lineTo(star[triangle[i][j]].obj.x, star[triangle[i][j]].obj.y);
				trianLines[i + finalStars/3].lineTo(star[triangle[i][j]].obj.x, star[triangle[i][j]].obj.y);

                star[triangle[i][j]].emitter = game.add.emitter(star[triangle[i][j]].obj.x, star[triangle[i][j]].obj.y, 100);
                star[triangle[i][j]].emitter.makeParticles('star');
                star[triangle[i][j]].emitter.minParticleSpeed.setTo(-400, -400);
                star[triangle[i][j]].emitter.maxParticleSpeed.setTo(400, 400);
                star[triangle[i][j]].emitter.maxParticleScale = 0.05;
                star[triangle[i][j]].emitter.maxParticleAlpha = 0.1;
                star[triangle[i][j]].emitter.gravity = 0;

				star[triangle[i][j]].obj.bringToTop();
				star[triangle[i][j]].text.bringToTop();
			}

			trianLines[i].alpha = 0;
			lineTween[i] = game.add.tween(trianLines[i]).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true, 0, 0, false);
            lineTween[i].pause();

			lineTween[i + finalStars/3] = game.add.tween(trianLines[i + finalStars/3]).to({alpha: 0}, 1000, "Linear", true, 0, -1, true);
            lineTween[i + finalStars/3].pause();

		}
        star.map(function (singleStar) {
            singleStar.obj.alpha = 0.2;
        })
	},this);
},

Formularium.Identificacion.prototype.update = function ()
{
	if (changeState){
		changeState = false;

		if (gameState.actual == "GAME"){

			gameState.actual = "PAUSE";
			pauseButton.bg.visible = true;
			pauseButton.bg.bringToTop();
			// pauseButton.statusText.visible = true;
			// pauseButton.statusText.setText('Level> '+game.level+' Stage> '+stage+' Fails> '+fails);
			// pauseButton.statusText.bringToTop();
			pauseButton.homeButton.visible = true;
			pauseButton.homeButton.bringToTop();
			pauseButton.exitButton.visible = true;
			pauseButton.exitButton.bringToTop();
			pauseButton.exitButton.inputEnabled = true;
			pauseButton.helpButton.visible = true;
			pauseButton.helpButton.bringToTop();
			pauseButton.helpButton.inputEnabled = true;

		}
		else if (gameState.actual == "PAUSE"){

			gameState.actual = "GAME";
			pauseButton.bg.visible = false;
			// pauseButton.statusText.visible = false;
			pauseButton.homeButton.visible = false;
			pauseButton.exitButton.visible = false;
			pauseButton.helpButton.visible = false;


			if(gameState.next == "HELP"){

				gameState.actual = "HELP";
				helpState.bg.visible = true;
				helpState.bg.bringToTop();
				helpState.exitButton.visible = true;
				helpState.exitButton.inputEnabled = true;
				helpState.exitButton.bringToTop();
				// helpState.helpText.visible = true;
				// helpState.helpText.bringToTop();
				helpState.relations.visible = true;
				helpState.relations.bringToTop();


			}
			else {
				pauseButton.button.inputEnabled = true;
			}
		}
		else if (gameState.actual == "HELP"){

			gameState.actual = "GAME";
			helpState.bg.visible = false;
			helpState.exitButton.visible = false;
			// helpState.helpText.visible = false;
			helpState.relations.visible = false;
			pauseButton.button.inputEnabled = true;
		}

		console.log("GameState: "+gameState.actual);
	}

	if (newQuestion && !intro.isRunning){

		newQuestion = false;
		// animation.option = true;
		question.text.setText('');

		for (var i=0; i<nOptions; i++){					//	elimina las opciones previas
			options[i].button.events.onInputDown.removeAll();
			options[i].button.inputEnabled = true;
			options[i].checkImage.destroy();
			options[i].button.x = game.width + options[i].button.width;
			options[i].text.x = options[i].button.x;
			question.text.x = options[i].button.x;
			game.add.tween(options[i].button).to({x: game.width*0.8}, 500, Phaser.Easing.Linear.None, true, 0, 0);
			game.add.tween(options[i].text).to({x: game.width*0.8}, 500, Phaser.Easing.Linear.None, true, 0, 0);
		}
		game.add.tween(question.text).to({x: game.width*0.8}, 500, Phaser.Easing.Linear.None, true, 0, 0);

        star.map(function(ra) {
            ra.obj.tint = "0xffffff";
        });
									//	SE PASO A LA SIGUIENTE ETAPA

		if (winLevel){						//	GANAR
			winLevel = false;
			// nextLvlButton = this.add.button(game.width*0.8, game.height*0.5,
			//  'button', this.nextLevel, this);
			// nextLvlButton.anchor.setTo(0.5, 0.5);
			// nextLvlButton.scale.setTo(0.5*proportions.min, 0.5*proportions.min);
			// nextLvlButton.text = this.add.text(nextLvlButton.x, nextLvlButton.y, '',
			// 	{ font: '20px Trebuchet MS', fill: '#fff', align: 'center' });
			// nextLvlButton.text.anchor.setTo(0.5, 0.5);
			// nextLvlButton.text.setText('Siguiente');
			// nextLvlButton.text.scale.setTo(proportions.min, proportions.min);
            this.resultState();
			// helpButton.destroy();

			for(var i = 0; i < nOptions ; i++){
				options[i].button.destroy();
				options[i].text.destroy();
			}
		} else {

			// if(addStars){
            //
			// 	//	FORMAR UN NUEVO TRIANGULO
			// 	questTriangle++;
			// 	addStars = false;
			// 	for(var i = 1; i < triangle[questTriangle].length; i++){
			// 		star[triangle[questTriangle][i]].obj.visible = true;
			// 		star[triangle[questTriangle][i]].text.visible = true;
			// 	}
			// 	trianLines[questTriangle].visible = true;
			// 	trianLines[questTriangle + finalStars/3].visible = true;
			// 	console.log("choosenQuestion: "+choosenQuestions);
            //
			// }
			let colores_opciones = [];

            star.map(function (x) {
                x.obj.alpha = 0.2;
                x.text.alpha = 0.2;
            });

            trianLines.map(function (x) {
                x.alpha = 0.2;
            });

            lineTween.map(function (x) {
                // x.stop(true);
                x.pause();
            });

            for (let i = 0; i < 3; i++) {
                star[triangle[Math.floor(choosenQuestions[0]/12)][i]].obj.alpha = 1;
                star[triangle[Math.floor(choosenQuestions[0]/12)][i]].text.alpha = 1;
            };

            trianLines[Math.floor(choosenQuestions[0]/12)].alpha = 1;
            lineTween[Math.floor(choosenQuestions[0]/12)].resume();
            trianLines[Math.floor(choosenQuestions[0]/12) + finalStars/3].alpha = 1;
            lineTween[Math.floor(choosenQuestions[0]/12) + finalStars/3].resume();

			if((choosenQuestions[0]%12)<2){
				// console.log("op "+star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].op);
				correctOption = 0;		//	hipotenusa
				question.text.setText(questionsPool[0].type+
					"Para el ángulo "+star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].id+
					"\nindique cuál lado es\nla hipotenusa.");
				question.text.addColor('#fff', 25);
				answers[0] = questionsPool[0].type+
					star[triangle[Math.floor(choosenQuestions[0]/12)][0]].id+star[triangle[Math.floor(choosenQuestions[0]/12)][1]].id;
				//options[0].text.addColor('#0f0', 0);
				colores_opciones[0] = '#0f0';
				answers[1] = questionsPool[0].type+
					star[star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].op[0]].id+
					star[star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].op[1]].id;
				catetoColor1 = (star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].op[0]%3) ? '#0aa' : '#f00';
				//options[1].text.addColor(catetoColor1, 0);
				colores_opciones[1] = catetoColor1;
				answers[2] = questionsPool[0].type+
					star[star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].adj[0]].id+
					star[star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].adj[1]].id;
				catetoColor2 = (star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].adj[0]%3) ? '#0aa' : '#f00';
				//options[2].text.addColor(catetoColor2, 0);
				colores_opciones[2] = catetoColor2;

                pistas.lista = [];

                pistas.lista.push("Acuerdate que para el ángulo " + star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].id + ",\n" +
                "la hipotenusa es el lado más largo del triángulo ");

			}
			else if((choosenQuestions[0]%12)<4){
				// console.log("adj "+triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]);
				correctOption = 1;		//	cateto opuesto
				question.text.setText(questionsPool[0].type+
					"Para el ángulo "+star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].id+
					"\nindique cuál lado es\nel cateto opuesto.");
				question.text.addColor('#fff', 25);
				answers[0] = questionsPool[0].type+
					star[triangle[Math.floor(choosenQuestions[0]/12)][0]].id+star[triangle[Math.floor(choosenQuestions[0]/12)][1]].id;
				//options[0].text.addColor('#0f0', 0);
				colores_opciones[0] = '#0f0';
				answers[1] = questionsPool[0].type+
					star[star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].op[0]].id+
					star[star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].op[1]].id;
				catetoColor1 = (star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].op[0]%3) ? '#0aa' : '#f00';
				//options[1].text.addColor(catetoColor1, 0);
				colores_opciones[1] = catetoColor1;
				answers[2] = questionsPool[0].type+
					star[star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].adj[0]].id+
					star[star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].adj[1]].id;
				catetoColor2 = (star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].adj[0]%3) ? '#0aa' : '#f00';
				//options[2].text.addColor(catetoColor2, 0);
				colores_opciones[2] = catetoColor2;

                pistas.lista = [];

                pistas.lista.push("Acuerdate que para el ángulo " + star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].id + ",\n" +
                "el cateto opuesto es el lado que no toca a el ángulo");
			}
			else if((choosenQuestions[0]%12)<6){
				correctOption = 2;		//	cateto adyacente
				question.text.setText(questionsPool[0].type+
					"Para el ángulo "+star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].id+
					"\nindique cuál lado es\nel cateto adyacente.");
				question.text.addColor('#fff', 25);
				answers[0] = questionsPool[0].type+
					star[triangle[Math.floor(choosenQuestions[0]/12)][0]].id+star[triangle[Math.floor(choosenQuestions[0]/12)][1]].id;
				//options[0].text.addColor('#0f0', 0);
				colores_opciones[0] = '#0f0';
				answers[1] = questionsPool[0].type+
					star[star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].op[0]].id+
					star[star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].op[1]].id;
				catetoColor1 = (star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].op[0]%3) ? '#0aa' : '#f00';
				//options[1].text.addColor(catetoColor1, 0);
				colores_opciones[1] = catetoColor1;
				answers[2] = questionsPool[0].type+
					star[star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].adj[0]].id+
					star[star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].adj[1]].id;
				catetoColor2 = (star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].adj[0]%3) ? '#0aa' : '#f00';
				//options[2].text.addColor(catetoColor2, 0);
				colores_opciones[2] = catetoColor2;

                pistas.lista = [];

                pistas.lista.push("Acuerdate que para el ángulo " + star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].id + ",\n" +
                "el cateto adyacente es el cateto que toca el ángulo, pero no es la hipotenusa");
			}
			//Segundo grupo de preguntas
			else if((choosenQuestions[0]%12)<8){
				correctOption = 0;		//	hipotenusa
				question.text.setText(questionsPool[0].type+
					"Para el ángulo "+star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].id+
					"\nel lado "+star[triangle[Math.floor(choosenQuestions[0]/12)][0]].id+
					star[triangle[Math.floor(choosenQuestions[0]/12)][1]].id+
					"\ncorresponde a:");
				question.text.addColor('#0f0', 25);
				question.text.addColor('#fff', 27);
				answers[0] = questionsPool[0].type+"Hipotenusa";
				colores_opciones[0] = '#fff';
				answers[1] = questionsPool[0].type+"Cateto Opuesto";
				colores_opciones[1] = '#fff';
				answers[2] = questionsPool[0].type+"Cateto Adyacente";
				colores_opciones[2] = '#fff';

                pistas.lista = [];

                pistas.lista.push("Acuerdate que para el ángulo " + star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].id + ",\n" +
                "la hipotenusa es el lado más largo del triángulo ");
			}
			else if((choosenQuestions[0]%12)<10){
				correctOption = 1;		//	cateto opuesto
				question.text.setText(questionsPool[0].type+
					"Para el ángulo "+star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].id+
					"\nel lado "+star[star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].op[0]].id+
					star[star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].op[1]].id+
					"\ncorresponde a:");
				catetoColor1 = (star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].op[0]%3) ? '#0aa' : '#f00';
				question.text.addColor(catetoColor1, 25);
				question.text.addColor('#fff', 27);
				answers[0] = questionsPool[0].type+"Hipotenusa";
				colores_opciones[0] = '#fff';
				answers[1] = questionsPool[0].type+"Cateto Opuesto";
				colores_opciones[1] = '#fff';
				answers[2] = questionsPool[0].type+"Cateto Adyacente";
				colores_opciones[2] = '#fff';

                pistas.lista = [];

                pistas.lista.push("Acuerdate que para el ángulo " + star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].id + ",\n" +
                "el cateto opuesto es el lado que no toca a el ángulo");
			}
			else{
				correctOption = 2;		//	cateto adyacente
				question.text.setText(questionsPool[0].type+
					"Para el ángulo "+star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].id+
					"\nel lado "+star[star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].adj[0]].id+
					star[star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].adj[1]].id+
					"\ncorresponde a:");
				catetoColor2 = (star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].adj[0]%3) ? '#0aa' : '#f00';
				question.text.addColor(catetoColor2, 25);
				question.text.addColor('#fff', 27);
				answers[0] = questionsPool[0].type+"Hipotenusa";
				colores_opciones[0] = '#fff';
				answers[1] = questionsPool[0].type+"Cateto Opuesto";
				colores_opciones[1] = '#fff';
				answers[2] = questionsPool[0].type+"Cateto Adyacente";
				colores_opciones[2] = '#fff';

                pistas.lista = [];

                pistas.lista.push("Acuerdate que para el ángulo " + star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].id + ",\n" +
                "el cateto adyacente es el cateto que toca\nel ángulo, pero no es la hipotenusa");
			};

            pistas.lista.push(
                "Acuerdate que las expresiones\n"+
                "trigonométricas se encuentran en\n"+
                "el menú de ayuda que se encuentra\n"+
                " al darle al botón de pausa");
            pistas.lista.push(
                "Acuerdate que la constelación\n"+
                "esta pintada de cierta forma,\n"+
                "el ángulo relativo a la pregunta\n"+
                "está de amarillo");
            pistas.lista.push(
                "Acuerdate que la constelación\n"+
                "esta pintada de cierta forma,\n"+
                "el color de los lados del enunciado\n"+
                "corresponden a los colores de los\n"+
                "trazos entre las estrellas");

            pistas.lista = this.shuffle(pistas.lista);
            pistas.text.setText("");

            star[triangle[Math.floor(choosenQuestions[0]/12)][choosenQuestions[0]%2]].obj.tint = "0xffff00";

			hold = this.shuffle(this.createOrderArray(0, answers.length));

			for (var i = 0; i < nOptions ; i++){
				options[i].text.setText(answers[hold[i]]);
				options[i].text.addColor(colores_opciones[hold[i]],0);
				if (hold[i] != correctOption){
					//	OPCION INCORRECTA
					options[i].button.events.onInputDown.add(this.wrongOption,this);
					console.log("wrong: "+i);
				}
				else{
					//	OPCION CORRECTA
					options[i].button.events.onInputDown.add(this.rightOption,this);
					console.log("good: "+i);
				}
			};
		};
	};


	if(rotateStars.en){
		for(var i=0; i <rotateStars.num; i++){
			star[rotateStars.theseStars[i]].obj.angle += 1;
		};
	};

},

	//	genera una matriz de dimension numbersNeeded de aleatorios no repetidos contando el 0 hasta el topLimit
Formularium.Identificacion.prototype.notRepeatRandom = function (topLimit, numbersNeeded)
{
	for (var i = 0, ar = []; i < topLimit; i++) {
	    ar[i] = i+1;
	}

	ar.sort(function () {
	    return Math.random() - 0.5;
	});
	var numbersNotRepeated = [];
	for (var i = 0; i<numbersNeeded; i++){
		numbersNotRepeated.push(ar[ar.length-1]);
		ar.pop();
	}
	return numbersNotRepeated;
},

Formularium.Identificacion.prototype.toggleCoordenates = function (star)
{
	if(onCoord){
		for(var i = 0;i<iStars; i++){
			star[i].text.setText(star[i].id);
		};
		onCoord = false;
	}
	else{
		for(var i = 0; i<iStars; i++){
			star[i].text.setText(star[i].id+':( '+star[i].x+', '+star[i].y+')');
		};
		onCoord = true;
	};
},

Formularium.Identificacion.prototype.wrongOption = function (boton)
{
	options[boton.number].checkImage = game.add.sprite(options[boton.number].x + 200*proportions.min, options[boton.number].y, 'wrong');
	options[boton.number].checkImage.anchor.setTo(0.5,0.5);
	options[boton.number].checkImage.scale.setTo(0.15*proportions.min,0.15*proportions.min);
	fails++;
	boton.inputEnabled = false;
	statusText.setText('Nivel: '+game.level+' Etapa: '+stage+'/'+ nStages + ' Fallos: '+fails);
	// pauseButton.statusText.setText('Level> '+game.level+' Stage> '+stage+'\nFails> '+fails);

    let hold = pistas.lista.shift();
    pistas.text.setText(hold);
    pistas.lista.push(hold);
},

Formularium.Identificacion.prototype.rightOption = function (boton)
{
	options[boton.number].checkImage = game.add.sprite(options[boton.number].x + 200*proportions.min, options[boton.number].y, 'right');
	options[boton.number].checkImage.anchor.setTo(0.5*proportions.min,0.5*proportions.min);
	options[boton.number].checkImage.scale.setTo(0.3,0.3);

	if(stage == nStages){

		winLevel = true;					//	GANAR

	} else {
		console.log("stage "+stage+" iStars "+iStars+" nStages "+nStages);
		if ((stage == iStars)&&(stage < nStages)){

			console.log("mas estrellas");
			addStars = true;
			iStars+=2;

		}

		stage++;
		choosenQuestions.shift();		//	elimina la primera pregunta ya hecha, sigue la secuencia
	}

    star[rotateStars.theseStars[rotateStars.num]].emitter.start(false, 200, 100);
	rotateStars.num++;
	// pauseButton.statusText.setText('Level> '+game.level+' Stage> '+stage+'\nFails> '+fails);
	newQuestion = true;
	statusText.setText('Nivel: '+game.level+' Etapa: '+stage+'/'+ nStages + ' Fallos: '+fails);

},

Formularium.Identificacion.prototype.nextLevel = function ()
{
	//YO MANUEL COMENTE ESTAS LINEAS
	// game.level++;
	// this.state.start('Parte1');
	var estrellas = 3-blackStars;
	if(game.global.starsArray[game.global.level-1]<estrellas)
	{
		game.global.starsArray[game.global.level-1] = estrellas;
		firebase.database().ref("/users/" + this.game.usuario.value + "/starsArray").set(game.global.starsArray);
	}
	let change = false;
	// if we completed a level and next level is locked - and exists - then unlock it
	if(estrellas>0 && game.global.starsArray[game.global.level]==4 && game.global.level<game.global.starsArray.length)
	{
		game.global.starsArray[game.global.level] = 0;
		change = true;
	}
	if (estrellas>0 && game.global.starsArray[game.global.level+3]==4 && game.global.level<game.global.starsArray.length-4)
	{
		game.global.starsArray[game.global.level+3] = 0;
		change = true;
	}
	if (change == true)
	{
		change = false;
		firebase.database().ref("/users/" + this.game.usuario.value + "/starsArray").set(game.global.starsArray);
	}
	this.game.state.start("BootState", true, false, "assets/levels/level_select.json", "LevelSelect");
},

Formularium.Identificacion.prototype.homeScreen = function ()
{
	//YO MANUEL COMENTE ESTA LINEA
	//this.state.start('Menu');
	this.game.state.start("BootState", true, false, "assets/levels/level_select.json", "LevelSelect");
},

Formularium.Identificacion.prototype.shuffle = function (array)
{
	var currentIndex = array.length, temporaryValue, randomIndex;

  	// While there remain elements to shuffle...
	while (0 !== currentIndex) {

	    // Pick a remaining element...
	    randomIndex = Math.floor(Math.random() * currentIndex);
	    currentIndex -= 1;

	    // And swap it with the current element.
	    temporaryValue = array[currentIndex];
	    array[currentIndex] = array[randomIndex];
	    array[randomIndex] = temporaryValue;
  	}

	return array;
},

Formularium.Identificacion.prototype.createOrderArray = function (init, finish)
{
	var array = [];
	for (var i = init; i < finish; i++){
		array[i - init] = i;
	}
	return array;
},

Formularium.Identificacion.prototype.changeState = function (buton)
{
	changeState = true;
	buton.inputEnabled = false;
	if (buton.use == "help"){
		console.log(buton.use);
		gameState.next = "HELP";
	}
	else if (buton.use == "exit"){
		console.log(buton.use);
		gameState.next = "GAME";
	}
},

Formularium.Identificacion.prototype.resultState = function () {
    let resultUI = {
        bg: 0,
        star: []
    };

    blackStars = fails;
    if (fails >= 1) {
        blackStars = 1;
    }
    if (fails >= 3) {
        blackStars = 2;
    }
    if (fails >= 6) {
        blackStars = 3;
    }

    resultUI.bg = game.add.sprite(game.width * 0.5, game.height *0.5, 'button');
    resultUI.bg.scale.setTo(proportions.min, 2*proportions.min);
    resultUI.bg.anchor.setTo(0.5);

    nextLvlButton = this.add.button(game.width*0.5, game.height*0.6, 'next', this.nextLevel, this);
    nextLvlButton.anchor.setTo(0.5, 0.5);
    nextLvlButton.scale.setTo(0.5*proportions.min);

    for (let i = 0; i < 3; i++) {
        resultUI.star[i] = game.add.sprite(game.width * 0.1 * (4 + i), game.height * (0.5 - ((i%2) * 0.1)), 'starScore');
        resultUI.star[i].scale.setTo(proportions.min*0.5);
        resultUI.star[i].anchor.setTo(0.5);
        if (3 - blackStars <= i) {
            resultUI.star[i].tint = "0x666666";
        }
    }
}
