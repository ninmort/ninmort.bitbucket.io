var Formularium = Formularium || {};

Formularium.Constelaciones2_tutorial = function () {
    "use strict";
    Phaser.State.call(this);
};

Formularium.Constelaciones2_tutorial.prototype = Object.create(Phaser.State.prototype);
Formularium.Constelaciones2_tutorial.prototype.constructor = Formularium.Constelaciones2_tutorial;

Formularium.Constelaciones2_tutorial.prototype.init = function ()
{
	//Modifico el tamano de la pantalla
	// this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
 //    this.game.scale.minWidth = 480;
 //    this.game.scale.minHeight = 260;
 //    this.game.scale.maxWidth = 1024;
 //    this.game.scale.maxHeight = 768;
 //    this.game.scale.pageAlignHorizontally = true;
 //    this.game.scale.pageAlignVertically = true;
    //this.game.scale.setScreenSize(true);

    this.game.scale.setGameSize(1270, 740);
    this.game.scale.pageAlignHorizontally = true;
	this.game.scale.pageAlignVertically = true;
	this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
	this.game.scale.updateLayout();
	//console.log("Prueba");
}

Formularium.Constelaciones2_tutorial.prototype.create = function ()
{
	// game.background = game.add.tileSprite(0, 0, game.world.width, game.world.height, 'spaceBG');
	// game.background.autoScroll(12, 24);
	game.level = game.global.level;
	game.stage.backgroundColor = '#061f27';
	gameBG = game.add.tileSprite(0, 0, game.width, game.height, 'spaceBG');
	gameBG.autoScroll(game.rnd.integerInRange(-5, 5), game.rnd.integerInRange(-5, 5));
	gameBG.alpha = 0.01*game.rnd.integerInRange(20,50);

	//	INICIALIZACION
	animation = {
		option: false,
		introText: true,
		fail: false
	};

    tutorial = [
        "Bienvenido a Constelaciones Desafio!",
        "Ahora es tu turno de completar la ecuación\ntrigonométrica. Según el lado dictado por\n"+
        "el enunciado y la expresión señalada, es tu\noportunidad de escoger las tres variables\n"+
        "(ángulo, numerador y denominador) que cumplan\nla ecuación.",
        "Presiona una de las variables y aparecerán\ntres opciones. Al darle clic a una de las \n"+
        "opciones, ésta se va a ver reflejada en la\nexpresión. Si deseas cambiar uno de los\n"+
        "valores previamente escogidos, puedes darle\nclic a la variable de nuevo.",
        "Para evaluar tus decisiones y comprobar tu\nresultado, puedes darle clic al botón de\n"+
        "\"Chequear\". Si es correcta, girará una\nnueva estrella en nuestra constelación.\n"+
        "Si es incorrecta, se te agregará un fallo\ny podrás intentar de nuevo."
    ];

	introText = 0;
    onCoord = false;		//	booleano para mostrar coordenadas
	star = [];				//	contiene las estrellas
	triangle = [[]];		//	contiene los triangulos
	nTriangles = 1;			//	cantidad de triangulos
	questTriangle = 0;		//	triangulo involucrado con la pregunta
	coord = [];
	options = [];			//	contiene los botones
	questOptions = [];
	questionsPool = [];		//	contiene las preguntas

	iStars = 3;				//	numero inicial de estrellas
	finalStars = 0;			//	numero final de estrellas
	winLevel = false;		//	indicador de nivel ganado
	nOptions = 3;			//	numero de opciones
	fails = 0;				//	cuenta la cantidad de fallos
	addStars = false;		//	agrega nueva estrellas

	nStages = 0;			//	numero de etapas
	stage = 1;				//	etapa actual
	newQuestion = true;	//	nueva pregunta para la nueva etapa
	var blackStars = 0;

    nextButton = {
        button: 0,
        image: 0,
        text: 0,
        currentState : 0,
        totalState: tutorial.length-1
    };
    lineTween = [];

	pauseButton = {
		button: 0,
		click: false,
		bg: 0,
		statusText: 0,
		homeButton: 0,
		helpButton: 0,
		exitButton: 0
	};

	helpState = {
		bg: 0,
		exitButton: 0,
		helpText: 0,
        relations: 0
	};

	gameState = {
		actual: "GAME",
		next: 0
	};
	changeState = false;

	rotateStars = {
		en: true,			//	habilita las estrellas girando
		theseStars: [],
		num: 0				//	cuantas estrellas giran que es igual a la etapa actual
	};

	// console.log("alto: "+game.width+"ancho: "+game.height);
	proportions = {
		x: game.width/1270,
		y: game.height/740,
		min: 0
	}

	if(proportions.x >= proportions.y){
		proportions.min = proportions.y;
	}
	else {
		proportions.min = proportions.x;
	}

	choosenQuestions = [];	//	secuencia de preguntas
	registeredClick = {
		click: false,
		button: 0,
	};
	finalAnswer = ['N', 'N', 'N'];	//	NO HA RESPONDIDO NIGUNA PREGUNTA
	questOptText = ["Ángulo", "Numerador", "Denominador"];
    color = [0xff0000, 0x00ff00, 0x00ffff];

	// statusText = this.add.text( game.width*0.8 , game.height*0.5 + 300*proportions.y, '',
	// 	{ font: '20px monospace', fill: '#fff', align: 'center' }
	// 	);
	// statusText.anchor.setTo(0.5, 0.5);
	// statusText.setText('Level> '+game.level+'\tStage> '+stage+'\nFails> '+fails);
	// statusText.scale.setTo(proportions.x, proportions.y);

	keyAnswers = [[]];		//	llave para decifrar cualquier pregunta
	readyButton = {
		button: 0,
		text: 0,
	};


	alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'];	//	nombre de las estrellas
	answers = [[]];

    pistas = {
        text: this.add.text(game.width*0.8, game.height, '',
			{ font: '20px Trebuchet MS', fill: '#fff', align: 'center' }),
        clue: []
    };
    pistas.text.anchor.setTo(0.5, 1);

	//	NIVEL
	// if(game.level <= 2){
	// 	finalStars = 9;
	// } else if(game.level <= 4){
	// 	finalStars = 6;
	// } else {
	// 	finalStars = 9;
	// }
    finalStars = 3;

	//	INICIALIZACION DE ESTRELLAS (UBICACION)
	spotsX = [];	//	almacena posiciones en X
	spotsY = [];	//	almacena posiciones en Y

	nStages = finalStars + 1 - (finalStars/3);		//	numero de etapas es igual al numero de estrellas
	hold = this.shuffle(this.createOrderArray(1, 10));
	spotsX = hold.slice(0, nStages);
	hold = this.shuffle(this.createOrderArray(1, 10));
	spotsY = hold.slice(0, nStages);
	spotsY = spotsY.sort();

	// spotsX = this.notRepeatRandom(8,finalStars);	//	poisicion de las estrellas en X
	// spotsY = this.notRepeatRandom(8,finalStars);	//	posicion de las estrellas en Y

	//	CONFIGURACION PARA AÑADIR LA TERCERA ESTRELLA
	if ((spotsX[0]>((spotsX[0]+spotsX[1])/2)) && (spotsY[0]>((spotsY[0]+spotsY[1])/2))
		||(spotsX[1]>((spotsX[0]+spotsX[1])/2)) && (spotsY[1]>((spotsY[0]+spotsY[1])/2))){
		hold = spotsX.slice(0,2);						//	obtiene los dos elementos del array
		hold.sort();									//
		spotsX[hold.length] = hold[hold.length-1];
		hold = spotsY.slice(0,2);						//	obtiene los dos elementos del array
		hold.sort();									//
		spotsY[hold.length] = hold[0];
	} else {
		hold = spotsY.slice(0,2);						//	obtiene los dos elementos del array
		hold.sort();									//
		spotsY[hold.length] = hold[0];
		hold = spotsX.slice(0,2);						//	obtiene los dos elementos del array
		hold.sort();									//
		spotsX[hold.length] = hold[0];
	}

	triangle[0] = [];		//	inicializa el primer triangulo
	hold = this.shuffle(this.createOrderArray(0, 3));	//	para que sea aleatorio el nombre de las estrellas

	for(var i = 0, j = 0, k = 0; i<finalStars; i++){				//	genera las estrellas * PENDIENTE DEL spotsX.length

		star[i] = {
			x: 0,						//	guarda las coordenadas en X
			y: 0,						//	guarda las coordenadas en Y
			obj: 1,								//	la estrella como asset
			text: 1,							//	el texto que se muestra como leyenda y coordenada
			id: 0,		//	le agrega un nombre
			adj: [],
			op: [],
            emitter: 0
		};

		if ((i == 0)||((i%3)!=0)){

			if(((i%3)!=2)||(i==2)){
				star[i].x = spotsX[k];
				star[i].y = spotsY[k];
				k++;
				// console.log("posicion a: "+i);
			}
			star[i].id = alphabet[j];
			rotateStars.theseStars[j] = i;
			j++;
			// console.log("letra a :" + i);
		}
		// console.log("i: "+i+" j: "+j+" k: "+k);

		star[i].obj = game.add.sprite(game.height*0.1*star[i].x, game.height*0.1*(10-star[i].y), 'star');
		star[i].obj.anchor.setTo(0.5, 0.5);
		star[i].obj.scale.setTo(0.5*proportions.min, 0.5*proportions.min);
		star[i].text = this.add.text(star[i].obj.x + 15*proportions.min, star[i].obj.y + 15*proportions.min, '',
			{ font: '20px Trebuchet MS', fill: '#fff', align: 'center' }
			);
		//star[i].text = game.add.bitmapText(star[i].obj.x + 15*proportions.min, star[i].obj.y + 15*proportions.min, "retroFont", "", 20);
        star[i].text = this.add.text(star[i].obj.x + 15*proportions.min, star[i].obj.y + 15*proportions.min, '',
			{ font: '20px Trebuchet MS', fill: '#fff', align: 'center' });
		star[i].text.anchor.setTo(0.5, 0.5);
		star[i].text.scale.setTo(proportions.min, proportions.min);
        star[i].text.visible = false;

		if(i >= iStars){
			star[i].obj.visible = false;

		}
		else{
			triangle[0][i] = i;
			// star[i].id = alphabet[hold[i]];
		}

		star[i].text.setText(star[i].id);

	};

	for(var i = 0; i<iStars; i++){			//	asigna cateto opuesto y adjacente

		if (i == (iStars-1)){
			star[i].adj = [triangle[questTriangle][0], triangle[questTriangle][1]];
			star[i].op = star[i].adj;
		}
		else {
			star[i].adj = [triangle[0][i], triangle[0][triangle[0].length-1]];
			hold = triangle[0].slice(0);				//	duplica el array triangle
			hold.splice(i,1);						//	elimina el cateto adjacente del array triangle
			star[i].op = [hold[0], triangle[0][triangle[0].length-1]];
			// console.log(i+" adj: "+star[i].adj+" op: "+star[i].op);
		}
	};


	//	BOTON DE CHEQUEO
	readyButton.button = game.add.sprite(game.width*0.8, game.height*0.68, 'button');
	readyButton.button.anchor.setTo(0.5, 0.5);
	readyButton.button.scale.setTo(0.5*proportions.min, 0.5*proportions.min);
	readyButton.button.inputEnabled = true;
	readyButton.button.events.onInputDown.add(this.checkAnswer, this);
	readyButton.text = this.add.text(game.width*0.8, game.height*0.68, 'Chequear',
		{ font: '20px Trebuchet MS', fill: '#fff', align: 'center' });
	readyButton.text.anchor.setTo(0.5, 0.5);
	readyButton.text.scale.setTo(proportions.min, proportions.min);
    readyButton.button.visible = false;
    readyButton.text.visible = false;
    readyButton.button.tint = "0x446600";


	//introText = game.add.bitmapText(game.width*0.5, game.height*0.5, "retroFont", "Nivel: "+game.level, 35);
    introText = this.add.text( game.width*0.5 , game.height*0.5, '',
		{ font: '20px Trebuchet MS', fill: '#fff', align: 'center' });
	introText.anchor.setTo(0.5, 0.5);
	introText.scale.setTo(4*proportions.min, 4*proportions.min);
	// introText.setText("LEVEL: "+game.level);

    nextButton.button = game.add.sprite(game.width * 0.8, game.height * 0.9, 'next');
    nextButton.button.anchor.setTo(0.5, 0.5);
    nextButton.button.scale.setTo(proportions.min*0.4, proportions.min*0.4);
    nextButton.button.inputEnabled = true;
    nextButton.button.events.onInputDown.add(this.nextTutorial, this);

    nextButton.text = this.add.text( game.width*0.8 , game.height*0.6, '',
		{ font: '20px Trebuchet MS', fill: '#fff', align: 'center' });
    nextButton.text.anchor.setTo(0.5, 0.5);
    nextButton.text.scale.setTo(proportions.min, proportions.min);
    nextButton.text.setText("¡Bienvenido a Constelaciones!\nPresiona el botón de abajo\npara continuar.\n"+
        nextButton.currentState+"/"+nextButton.totalState);

	// nextButton.image = game.add.sprite(game.width*0.8, game.height*0.2, '');
	// nextButton.image.anchor.setTo(0.5, 0.5);

	//	BOTON DE PAUSA
	pauseButton.button = game.add.sprite(game.width, 0, 'pause');
	pauseButton.button.anchor.setTo(1, 0);
	pauseButton.button.scale.setTo(0.1*proportions.min, 0.1*proportions.min);
	pauseButton.button.inputEnabled = true;
	pauseButton.button.events.onInputDown.add(this.changeState, this);

	pauseButton.bg = game.add.sprite(game.width*0.5, game.height*0.5, 'button');
	pauseButton.bg.anchor.setTo(0.5, 0.5);
	pauseButton.bg.scale.setTo(1*proportions.min, proportions.min);
	pauseButton.bg.visible = false;

	// pauseButton.statusText = this.add.text( game.width*0.5 , game.height*0.5 - 50*proportions.min, '',
	// 	{ font: '20px Trebuchet MS', fill: '#fff', align: 'center' });
	// pauseButton.statusText.anchor.setTo(0.5, 0.5);
	// pauseButton.statusText.scale.setTo(proportions.min, proportions.min);
	// pauseButton.statusText.visible = false;

	pauseButton.homeButton = game.add.sprite(game.width*0.5 , game.height*0.5, 'home');
	pauseButton.homeButton.anchor.setTo(0.5, 0.5);
	pauseButton.homeButton.scale.setTo(0.25*proportions.min, 0.25*proportions.min);
	pauseButton.homeButton.inputEnabled = true;
	pauseButton.homeButton.events.onInputDown.add(this.homeScreen, this);
	pauseButton.homeButton.visible = false;

	pauseButton.exitButton = game.add.sprite(game.width*0.5 + 100*proportions.min, game.height*0.5, 'wrong');
	pauseButton.exitButton.anchor.setTo(0.5, 0.5);
	pauseButton.exitButton.scale.setTo(0.15*proportions.min, 0.15*proportions.min);
	pauseButton.exitButton.events.onInputDown.add(this.changeState, this);
	pauseButton.exitButton.visible = false;
	pauseButton.exitButton.use = "exit"

	pauseButton.helpButton = game.add.sprite(game.width*0.5 - 100*proportions.min, game.height*0.5, 'help');
	pauseButton.helpButton.anchor.setTo(0.5, 0.5);
	pauseButton.helpButton.scale.setTo(0.25*proportions.min, 0.25*proportions.min);
	pauseButton.helpButton.events.onInputDown.add(this.changeState, this);
	pauseButton.helpButton.visible = false;
	pauseButton.helpButton.use = "help";

	helpState.bg = game.add.sprite(game.width*0.5, game.height*0.5, 'button');
	helpState.bg.anchor.setTo(0.5, 0.5);
	helpState.bg.scale.setTo(1.5*proportions.min, 3*proportions.min);
	helpState.bg.visible = false;

	helpState.exitButton = game.add.sprite(game.width*0.5, game.height*0.2, 'wrong');
	helpState.exitButton.anchor.setTo(0.5, 0.5);
	helpState.exitButton.scale.setTo(0.15*proportions.min, 0.15*proportions.min);
	helpState.exitButton.events.onInputDown.add(this.changeState, this);
	helpState.exitButton.visible = false;

    helpState.relations = game.add.sprite(game.width*0.5, game.height*0.5, 'relation');
	helpState.relations.scale.setTo(proportions.min, proportions.min);
	helpState.relations.anchor.setTo(0.5, 0.5);
	helpState.relations.visible = false;
    helpState.relations.tint = "0x000000";

	// helpState.helpText = this.add.text( game.width*0.5 , game.height*0.5, '',
	// 	{ font: '20px Trebuchet MS', fill: '#fff', align: 'center' });
	// helpState.helpText.anchor.setTo(0.5, 0.5);
	// helpState.helpText.scale.setTo(proportions.min, proportions.min);
	// helpState.helpText.visible = false;
	// helpState.helpText.setText("La segunda parte del juego consite en: \n" +
	// 	"dependiendo de la expresión señalada y el lado que se pida\n" +
	// 	"es necesario formar la ecuación indicada. \n" +
	// 	"Se selecciona cuál parte de la ecuación se desea llenar (sea el ángulo, numerador \n" +
	// 	"o denominador) y luego se escoge una de las opciones. La opción escogida se va a ver \n" +
	// 	"reflejada en la ecuación. Al finalizar la ecuación se puede presionar el botón de Chequear. \n " +
	// 	"Si la respuesta es correcta, entonces se pasa a la siguiente etapa y \n" +
	// 	"una de las estrellas empezara a girar. Pasaras de nivel cuando todas \n" +
	// 	"las estrellas giren. \n " +
	// 	"Falla dos preguntas y tendras que resolver algo para seguir intentando.");

	// statusText = game.add.bitmapText(game.width*0.8, game.height*0.1, "retroFont", "", 20);
	// // statusText.text.align = "center";
	// statusText.align = "center";
	// statusText.alpha = 0;
	// statusText.anchor.setTo(0.5, 0.5);
    statusText = this.add.text( game.width*0.8 , game.height*0.05, '',
		{ font: '20px Trebuchet MS', fill: '#fff', align: 'center' });
	statusText.anchor.setTo(0.5, 0);
	statusText.setText('Nivel: '+game.level+' Etapa: '+stage+'/'+ nStages + ' Fallos: '+fails);
	statusText.scale.setTo(proportions.x, proportions.y);

	//	PREGUNTAS

	// choosenQuestions = this.notRepeatRandom(((iStars-1)*nOptions)-1,iStars);	//	se escogen las preguntas del nivel aleatorio
	choosenQuestions = this.shuffle(this.createOrderArray(0, (iStars-1)*nOptions));
	console.log("choosenQuestion> "+choosenQuestions);

	question = {		//	formato
		num: 0,
		enunciado: this.add.text(
			game.width*0.8, game.height*0.2,
			'', { font: '20px Trebuchet MS', fill: '#fff', align: 'center' }),
		expresion: this.add.text(
			(game.width*0.8) - 150*proportions.min, game.height*0.4,
			'', { font: '20px Trebuchet MS', fill: '#fff', align: 'center' }),
	};
	question.enunciado.anchor.setTo(0.5, 0.5);
	question.enunciado.scale.setTo(proportions.min, proportions.min);
	question.expresion.anchor.setTo(0.5, 0.5);
	question.expresion.scale.setTo(proportions.min, proportions.min);
    question.enunciado.visible = false;
    question.expresion.visible = false;

	//	BOTONES PARA LA PREGUNTA
	for (var i = 0; i < nOptions; i++){
		questOptions[i] = {
			x: 0,
			y: 0,
			click: false,
			text: 0,
			button: 0,
		};
		if (i == 0){
			questOptions[i].x = game.width*0.78;
			questOptions[i].y = game.height*0.4;
		}
		else {
			questOptions[i].x = game.width*0.93;
			questOptions[i].y = game.height*0.4 + ((i*2)-3)*35*proportions.min;
		};

        eq = game.add.sprite(game.width*0.82, game.height*0.4, 'eq');
        eq.anchor.setTo(0.5);
        eq.scale.setTo(0.8,  0.8);
        eq.visible = false;

		questOptions[i].button = game.add.sprite(questOptions[i].x, questOptions[i].y, 'button');
		questOptions[i].button.anchor.setTo(0.5, 0.5);
		questOptions[i].button.scale.setTo(0.25*proportions.min, 0.25*proportions.min);
		questOptions[i].button.inputEnabled = true;
		questOptions[i].button.number = i;
		questOptions[i].button.events.onInputDown.add(this.clickQuestOptions,this);
		questOptions[i].text = this.add.text(questOptions[i].x, questOptions[i].y, '',
			{ font: '20px Trebuchet MS', fill: '#fff', align: 'center' }
			);
		questOptions[i].text.anchor.setTo(0.5, 0.5);
		questOptions[i].text.setText(questOptText[i]);
		questOptions[i].text.scale.setTo(proportions.min, proportions.min);

        questOptions[i].text.visible = false;
        questOptions[i].button.visible = false;

	}

	//	BOTONES PARA LAS RESPUESTAS
	for (var i = 0; i < nOptions; i++){		//	genera las opciones

		options[i] = {
			x: game.width*0.8+(170*(i-1))*proportions.min,		//	EDITAR: no tiene sentido
			y: game.height*0.55,
			checkImage: 0,
			button: 0,
			text: 0,
		};

		options[i].button = game.add.sprite(options[i].x, options[i].y, 'button');
		options[i].button.anchor.setTo(0.5, 0.5);
		options[i].button.scale.setTo(0.25*proportions.min, 0.25*proportions.min);
		options[i].button.inputEnabled = true;
		options[i].button.number = i;
        options[i].button.tint = "0x446600";

		options[i].text = this.add.text(options[i].x, options[i].y, '',
			{ font: '20px Trebuchet MS', fill: '#fff', align: 'center' }
			);
		options[i].text.anchor.setTo(0.5, 0.5);
		options[i].text.scale.setTo(proportions.min, proportions.min);
		// options[i].text.setText(answers[i]);
		options[i].checkImage = game.add.sprite(200, 200, '');	//	QUITAR
		options[i].button.visible = false;

	};

	// choosenQuestions.shift();		//	elimina la primera pregunta ya hecha, sigue la secuencia
	failSign = game.add.sprite(game.width*0.8, game.height*0.5, 'wrong');
	failSign.anchor.setTo(0.5, 0.5);
	failSign.scale.setTo(proportions.min, proportions.min);
	failSign.visible = false;

	// trianLines = [];
	// trianLines[questTriangle] = game.add.graphics(0, 0);
	// trianLines[questTriangle].lineStyle(5, 0xff6600, 0.5);
	// trianLines[questTriangle].moveTo(star[triangle[questTriangle][(triangle[questTriangle].length-1)]].obj.x,
	// 	star[triangle[questTriangle][(triangle[questTriangle].length-1)]].obj.y);
    //
	// trianLines[questTriangle + finalStars/3] = game.add.graphics(0, 0);
	// trianLines[questTriangle + finalStars/3].lineStyle(2, 0xffffff, 1);
	// trianLines[questTriangle + finalStars/3].moveTo(star[triangle[questTriangle][(triangle[questTriangle].length-1)]].obj.x,
	// 	star[triangle[questTriangle][(triangle[questTriangle].length-1)]].obj.y);
    //
    // var i = 0; i < iStars ; i++){
 // 		trianLines[questTriangle].lineTo(star[triangle[questTriangle][i]].obj.x, star[triangle[questTriangle][i]].obj.y);
 // 		trianLines[questTriangle + finalStars/3].lineTo(star[triangle[questTriangle][i]].obj.x, star[triangle[questTriangle][i]].obj.y);
    //

 	for (var i = 0; i < iStars ; i++){		//	ultimo render sea la estrella
 		star[triangle[questTriangle][i]].obj.bringToTop();
 		star[triangle[questTriangle][i]].obj.alpha = 0;
 		game.add.tween(star[triangle[questTriangle][i]].obj).to({alpha: 1}, 1000, 'Linear', true);
 		hold = game.rnd.integerInRange(0, 30);
 		game.add.tween(star[triangle[questTriangle][i]].obj.scale).to({x: 0.5 + hold*0.01, y: 0.5 + hold*0.01}, 1000 + game.rnd.integerInRange(0, 500), 'Linear', true, game.rnd.integerInRange(0, 200), -1, true);
 		// star[triangle[questTriangle][i]].text.bringToTop();
 	}

    // add.tween(trianLines[questTriangle + finalStars/3]).to({alpha: 0}, 1000, "Linear", true, 0, -1, true);

 	if(finalStars/3 > (questTriangle+1)){
 		addStars = true;
 	}

	while(addStars){

		questTriangle++;
		// addStars = false;
		triangle[questTriangle] = [];

		// hold = this.shuffle(this.createOrderArray(0, 3));	//	aleatorio entre las estrellas del triangulo previo
		hold = [];
		if (star[triangle[questTriangle-1][0]].y > star[triangle[questTriangle-1][1]].y){
			hold[0] = 0;
		} else {
			hold[0] = 1;
		}
		// console.log("nombre star repeat: "+star[triangle[questTriangle-1][hold]].id);
		triangle[questTriangle][0] = questTriangle*3;
		star[triangle[questTriangle][0]].id = star[triangle[questTriangle-1][hold[0]]].id;
		star[triangle[questTriangle][0]].x = star[triangle[questTriangle-1][hold[0]]].x;
		star[triangle[questTriangle][0]].y = star[triangle[questTriangle-1][hold[0]]].y;
		star[triangle[questTriangle][0]].obj.x = star[triangle[questTriangle-1][hold[0]]].obj.x;
		star[triangle[questTriangle][0]].obj.y = star[triangle[questTriangle-1][hold[0]]].obj.y;

		triangle[questTriangle][1] = 1 + questTriangle*3;
		star[triangle[questTriangle][1]].obj.visible = true;
		star[triangle[questTriangle][1]].text.visible = false;

		triangle[questTriangle][2] = 2 + questTriangle*3;
		star[triangle[questTriangle][2]].x = star[triangle[questTriangle][0]].x;		//	MODIFICAR
		star[triangle[questTriangle][2]].y = star[triangle[questTriangle][1]].y;		//	MODIFICAR
		star[triangle[questTriangle][2]].obj.x = star[triangle[questTriangle][2]].x*game.height*0.1;
		star[triangle[questTriangle][2]].obj.y = (10-star[triangle[questTriangle][2]].y)*game.height*0.1;
		star[triangle[questTriangle][2]].text.x = star[triangle[questTriangle][2]].obj.x + 15*proportions.min;
		star[triangle[questTriangle][2]].text.y = star[triangle[questTriangle][2]].obj.y + 15*proportions.min;
		star[triangle[questTriangle][2]].obj.visible = true;
		star[triangle[questTriangle][2]].text.visible = false;

		for(var i = 0; i<triangle[questTriangle].length; i++){			//	asigna cateto opuesto y adjacente
			if (i == triangle[questTriangle].length-1){
				star[triangle[questTriangle][i]].adj = [triangle[questTriangle][0], triangle[questTriangle][1]];
				star[triangle[questTriangle][i]].op = [triangle[questTriangle][0], triangle[questTriangle][1]];
			}
			else {
				star[triangle[questTriangle][i]].adj = [triangle[questTriangle][i], triangle[questTriangle][triangle[questTriangle].length-1]];
				hold = triangle[questTriangle].slice(0);				//	duplica el array triangle
				hold.splice(i,1);						//	elimina el cateto adjacente del array triangle
				star[triangle[questTriangle][i]].op = [hold[0], triangle[questTriangle][triangle[questTriangle].length-1]];
			}
			// console.log(triangle[questTriangle][i]+" : adj: "+star[triangle[questTriangle][i]].adj + " op: " + star[triangle[questTriangle][i]].op);
		};

		// trianLines[questTriangle] = game.add.graphics(0, 0);
		// trianLines[questTriangle].lineStyle(5, 0xff6600, 0.5);
		// trianLines[questTriangle].moveTo(star[triangle[questTriangle][(triangle[questTriangle].length-1)]].obj.x,
		// 	star[triangle[questTriangle][(triangle[questTriangle].length-1)]].obj.y);
        //
		// trianLines[questTriangle + finalStars/3] = game.add.graphics(0, 0);
		// trianLines[questTriangle + finalStars/3].lineStyle(2, 0xffffff, 1);
		// trianLines[questTriangle + finalStars/3].moveTo(star[triangle[questTriangle][(triangle[questTriangle].length-1)]].obj.x,
		// 	star[triangle[questTriangle][(triangle[questTriangle].length-1)]].obj.y);
        //
        //  i = 0; i < iStars ; i++){
        // nes[questTriangle].lineTo(star[triangle[questTriangle][i]].obj.x, star[triangle[questTriangle][i]].obj.y);
        // nes[questTriangle + finalStars/3].lineTo(star[triangle[questTriangle][i]].obj.x, star[triangle[questTriangle][i]].obj.y);
        //

        //  i = 0; i < iStars ; i++){		//	ultimo render sea la estrella
        // iangle[questTriangle][i]].obj.bringToTop();
        // iangle[questTriangle][i]].obj.alpha = 0;
        // d.tween(star[triangle[questTriangle][i]].obj).to({alpha: 1}, 1000, 'Linear', true);
        // game.rnd.integerInRange(0, 30);
        // d.tween(star[triangle[questTriangle][i]].obj.scale).to({x: 0.5 + hold*0.01, y: 0.5 + hold*0.01}, 1000 + game.rnd.integerInRange(0, 500), 'Linear', true, game.rnd.integerInRange(0, 200), -1, true);
        // [triangle[questTriangle][i]].text.bringToTop();
        //
        //
        // es[questTriangle].visible = false;
        // es[questTriangle + finalStars/3].visible = false;
        // .tween(trianLines[questTriangle + finalStars/3]).to({alpha: 0}, 1000, "Linear", true, 0, -1, true);



	 	if(finalStars/3 == (questTriangle+1)){
 			addStars = false;
 		}
	}

	questTriangle = 0;

    let sortedPos = {
		x: [],
		y: []
	}

	for (var i = 0, n = star.length; i < n ; i++){		//	ultimo render sea la estrella
	 	star[i].obj.bringToTop();
		if (star[i].x !== 0  && star[i].y !== 0) {
			sortedPos.x.push(star[i].x);
			sortedPos.y.push(star[i].y);
		}
		// star[triangle[questTriangle][i]].text.bringToTop();
	 }

	 sortedPos.x.sort();
	 sortedPos.y.sort();

	 var propStar = (sortedPos.x[sortedPos.x.length-1] - sortedPos.x[0]) + 1;
		 if (propStar < (sortedPos.y[sortedPos.y.length-1] - sortedPos.y[0] + 1)) {
			 propStar = (sortedPos.y[sortedPos.y.length-1] - sortedPos.y[0] + 1);
		 }

	 for (var i = 0, n = star.length; i < n; i++) {
		 game.add.tween(star[i].obj).to({x: game.height * ((star[i].x - sortedPos.x[0])/propStar + 0.1)}, 1000, Phaser.Easing.Linear.None, true, 0, 0, false);

			game.add.tween(star[i].obj).to({y: game.height * ((1 - (star[i].y - sortedPos.y[0])/propStar) - 0.1)}, 1000, Phaser.Easing.Linear.None, true, 0, 0, false);
	 }

	for (var i = 0; i < star.length ; i++){		//	ultimo render sea la estrella
	 	star[i].obj.bringToTop();
		// star[triangle[questTriangle][i]].text.bringToTop();
	 }

	// choosenQuestions.shift();		//	elimina la primera pregunta ya hecha, sigue la secuencia
	var intro = game.add.tween(introText).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true, 0, 0, false);
	game.add.tween(statusText).to({alpha: 1}, 1000, Phaser.Easing.Linear.None, true, 0, 0, false);

    // intro.onComplete.add(function() {
	// 	trianLines = [];
	// 	for (var i = 0, n = triangle.length; i < n; i++) {
	// 		trianLines[i] = game.add.graphics(0, 0);
	// 		trianLines[i].lineStyle(5, 0xff6600, 0.5);
	// 		trianLines[i].moveTo(star[triangle[i][(triangle[i].length-1)]].obj.x,
	// 			star[triangle[i][(triangle[i].length-1)]].obj.y);
	// 		// trianLines[questTriangle].visible = true;
    //
	// 		trianLines[i + finalStars/3] = game.add.graphics(0, 0);
	// 		trianLines[i + finalStars/3].lineStyle(2, 0xffffff, 1);
	// 		trianLines[i + finalStars/3].moveTo(star[triangle[i][(triangle[i].length-1)]].obj.x,
	// 			star[triangle[i][(triangle[i].length-1)]].obj.y);
	// 		trianLines[i + finalStars/3].visible = true;
    //
	// 		for (var j = 0; j < 3; j++) {
	// 			star[triangle[i][j]].text.x = star[triangle[i][j]].obj.x + 30*proportions.min;
	// 			star[triangle[i][j]].text.y = star[triangle[i][j]].obj.y + 30*proportions.min;
	// 			star[triangle[i][j]].text.visible = true;
	// 			star[triangle[i][j]].text.alpha = 0;
	// 			game.add.tween(star[triangle[i][j]].text).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true, 0, 0, false);
    //
	// 			trianLines[i].lineStyle(5, color[j], 0.5);
	// 			trianLines[i].lineTo(star[triangle[i][j]].obj.x, star[triangle[i][j]].obj.y);
	// 			trianLines[i + finalStars/3].lineTo(star[triangle[i][j]].obj.x, star[triangle[i][j]].obj.y);
    //
    //
	// 			star[triangle[i][j]].obj.bringToTop();
	// 			star[triangle[i][j]].text.bringToTop();
	// 		}
    //
	// 		trianLines[i].alpha = 0;
	// 		game.add.tween(trianLines[i]).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true, 0, 0, false);
    //
	// 		game.add.tween(trianLines[i + finalStars/3]).to({alpha: 0}, 1000, "Linear", true, 0, -1, true);
    //
	// 	}
	// },this);
    intro.onComplete.add(function() {
		trianLines = [];
		for (var i = 0, n = triangle.length; i < n; i++) {
			trianLines[i] = game.add.graphics(0, 0);
			trianLines[i].lineStyle(5, 0xff6600, 0.5);
			trianLines[i].moveTo(star[triangle[i][(triangle[i].length-1)]].obj.x,
				star[triangle[i][(triangle[i].length-1)]].obj.y);
			// trianLines[questTriangle].visible = true;

			trianLines[i + finalStars/3] = game.add.graphics(0, 0);
			trianLines[i + finalStars/3].lineStyle(2, 0xffffff, 1);
			trianLines[i + finalStars/3].moveTo(star[triangle[i][(triangle[i].length-1)]].obj.x,
				star[triangle[i][(triangle[i].length-1)]].obj.y);
			trianLines[i + finalStars/3].visible = true;

			for (var j = 0; j < 3; j++) {
				star[triangle[i][j]].text.x = star[triangle[i][j]].obj.x + 30*proportions.min;
				star[triangle[i][j]].text.y = star[triangle[i][j]].obj.y + 30*proportions.min;
				star[triangle[i][j]].text.visible = true;
				star[triangle[i][j]].text.alpha = 0;
				game.add.tween(star[triangle[i][j]].text).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true, 0, 0, false);

				trianLines[i].lineStyle(5, color[j], 0.5);
				trianLines[i].lineTo(star[triangle[i][j]].obj.x, star[triangle[i][j]].obj.y);
				trianLines[i + finalStars/3].lineTo(star[triangle[i][j]].obj.x, star[triangle[i][j]].obj.y);

                star[triangle[i][j]].emitter = game.add.emitter(star[triangle[i][j]].obj.x, star[triangle[i][j]].obj.y, 100);
                star[triangle[i][j]].emitter.makeParticles('star');
                star[triangle[i][j]].emitter.minParticleSpeed.setTo(-400, -400);
                star[triangle[i][j]].emitter.maxParticleSpeed.setTo(400, 400);
                star[triangle[i][j]].emitter.maxParticleScale = 0.05;
                star[triangle[i][j]].emitter.maxParticleAlpha = 0.1;
                star[triangle[i][j]].emitter.gravity = 0;

				star[triangle[i][j]].obj.bringToTop();
				star[triangle[i][j]].text.bringToTop();
			}

			trianLines[i].alpha = 0;
			// game.add.tween(trianLines[i]).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true, 0, 0, false);

			// game.add.tween(trianLines[i + finalStars/3]).to({alpha: 0}, 1000, "Linear", true, 0, -1, true);
            lineTween[i] = game.add.tween(trianLines[i]).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true, 0, 0, false);
            // lineTween[i].pause();

			lineTween[i + finalStars/3] = game.add.tween(trianLines[i + finalStars/3]).to({alpha: 0}, 1000, "Linear", true, 0, -1, true);
            // lineTween[i + finalStars/3].pause();


		}
        // star.map(function (singleStar) {
        //     singleStar.obj.alpha = 0.2;
        // });

	},this);
},

Formularium.Constelaciones2_tutorial.prototype.update = function ()
{
	if(animation.option){
			if(question.enunciado.x > game.width*0.8){
				question.enunciado.x -= 20;
				question.expresion.x -= 20;
				for (var i = 0; i < questOptions.length; i++){
					questOptions[i].button.x -= 20;
					questOptions[i].text.x -= 20;
				}
			}
			else{
				animation.option = false;
			}
		};

		if(animation.fail){
			animation.fail = false;
			failSign.visible = true;
			failSign.alpha = 1;
			game.add.tween(failSign).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true, 0, 0, false);
		}

		if (changeState){
			changeState = false;

			if (gameState.actual == "GAME"){

				gameState.actual = "PAUSE";
				pauseButton.bg.visible = true;
				pauseButton.bg.bringToTop();
				// pauseButton.statusText.visible = true;
				// pauseButton.statusText.setText('Level> '+game.level+' Stage> '+stage+' Fails> '+fails);
				// pauseButton.statusText.bringToTop();
				pauseButton.homeButton.visible = true;
				pauseButton.homeButton.bringToTop();
				pauseButton.exitButton.visible = true;
				pauseButton.exitButton.bringToTop();
				pauseButton.exitButton.inputEnabled = true;
				pauseButton.helpButton.visible = true;
				pauseButton.helpButton.bringToTop();
				pauseButton.helpButton.inputEnabled = true;

			}
			else if (gameState.actual == "PAUSE"){

				gameState.actual = "GAME";
				pauseButton.bg.visible = false;
				// pauseButton.statusText.visible = false;
				pauseButton.homeButton.visible = false;
				pauseButton.exitButton.visible = false;
				pauseButton.helpButton.visible = false;


				if(gameState.next == "HELP"){

					gameState.actual = "HELP";
					helpState.bg.visible = true;
					helpState.bg.bringToTop();
					helpState.exitButton.visible = true;
					helpState.exitButton.inputEnabled = true;
					helpState.exitButton.bringToTop();
					// helpState.helpText.visible = true;
					// helpState.helpText.bringToTop();
                    helpState.relations.visible = true;
					helpState.relations.bringToTop();


				}
				else {
					pauseButton.button.inputEnabled = true;
				}
			}
			else if (gameState.actual == "HELP"){

				gameState.actual = "GAME";
				helpState.bg.visible = false;
				helpState.exitButton.visible = false;
				// helpState.helpText.visible = false;
                helpState.relations.visible = false;
				pauseButton.button.inputEnabled = true;
			}

			console.log("GameState: "+gameState.actual);
		};

		if (registeredClick.click){

			registeredClick.click = false;

			for(var i = 0; i < nOptions; i++){

				// questOptions[i].button.tint = "0xffffff";
				options[i].button.visible = true;
                options[i].text.visible = true;
				options[i].button.events.onInputDown.removeAll();
				options[i].button.events.onInputDown.add(this.clickedOption, this);
                // options[i].button.tint ="0xffffff";
                options[i].text.addColor("#fff", 0);

				if(registeredClick.button == 0){
					options[i].text.setText(star[triangle[Math.floor(choosenQuestions[0]/6)][answers[registeredClick.button][i]]].id);
				}
				else {

					if (registeredClick.button == 1){		//	NUMERADOR

						if (correctOption == 0){
							// console.log(star[star[triangle[Math.floor(choosenQuestions[0]/6)][answers[registeredClick.button][i]]].op[0]].id+
							// 	star[star[triangle[Math.floor(choosenQuestions[0]/6)][answers[registeredClick.button][i]]].op[1]].id);
							options[i].text.setText(star[star[triangle[Math.floor(choosenQuestions[0]/6)][answers[registeredClick.button][i]]].op[0]].id+
								star[star[triangle[Math.floor(choosenQuestions[0]/6)][answers[registeredClick.button][i]]].op[1]].id);

                            // console.log("star: " + star[triangle[Math.floor(choosenQuestions[0]/6)][answers[registeredClick.button][i]]].op[0] + " and cateto: "+star[triangle[Math.floor(choosenQuestions[0]/6)][answers[registeredClick.button][i]]].op[0]%3);

                            if (star[triangle[Math.floor(choosenQuestions[0]/6)][answers[registeredClick.button][i]]].op[1]%3 === 2) {
                                if (star[triangle[Math.floor(choosenQuestions[0]/6)][answers[registeredClick.button][i]]].op[0]%3 === 0) {
                                    // options[i].button.tint = "0xff0000";
                                    options[i].text.addColor("#f00", 0);
                                    // console.log("red "+i);
                                }
                                else if (star[triangle[Math.floor(choosenQuestions[0]/6)][answers[registeredClick.button][i]]].op[0]%3 === 1) {
                                    // options[i].button.tint = "0x0000ff";
                                    options[i].text.addColor("#0aa", 0);
                                    // console.log("blue "+i);
                                }

                            }

                            else {
                                options[i].text.addColor("#0f0", 0);
                            }

							// options[i].text.setText(star[triangle[Math.floor(choosenQuestions[0]/6)][answers[registeredClick.button][i]]].op[0]+""+
							// 	star[triangle[Math.floor(choosenQuestions[0]/6)][answers[registeredClick.button][i]]].op[1]);

						}
						else {
							// console.log(star[star[triangle[Math.floor(choosenQuestions[0]/6)][answers[registeredClick.button][i]]].adj[0]].id+
							// 	star[star[triangle[Math.floor(choosenQuestions[0]/6)][answers[registeredClick.button][i]]].adj[1]].id);
							options[i].text.setText(star[star[triangle[Math.floor(choosenQuestions[0]/6)][answers[registeredClick.button][i]]].adj[0]].id+
								star[star[triangle[Math.floor(choosenQuestions[0]/6)][answers[registeredClick.button][i]]].adj[1]].id);

                            if (star[triangle[Math.floor(choosenQuestions[0]/6)][answers[registeredClick.button][i]]].adj[1]%3 === 2) {
                                if (star[triangle[Math.floor(choosenQuestions[0]/6)][answers[registeredClick.button][i]]].adj[0]%3 === 0) {
                                    // options[i].button.tint = "0xff0000";
                                    options[i].text.addColor("#f00", 0);
                                    // console.log("red "+i);
                                }
                                else if (star[triangle[Math.floor(choosenQuestions[0]/6)][answers[registeredClick.button][i]]].adj[0]%3 === 1) {
                                    // options[i].button.tint = "0x0000ff";
                                    options[i].text.addColor("#0aa", 0);
                                    // console.log("blue "+i);
                                }

                            }

                            else {
                                options[i].text.addColor("#0f0", 0);
                            }
							// options[i].text.setText(star[triangle[Math.floor(choosenQuestions[0]/6)][answers[registeredClick.button][i]]].adj[0]+""+
							// 	star[triangle[Math.floor(choosenQuestions[0]/6)][answers[registeredClick.button][i]]].adj[1]);
						};
					}
					else {			//	DENOMINADOR
						// console.log(star[star[triangle[Math.floor(choosenQuestions[0]/6)][answers[registeredClick.button][i]]].adj[0]].id+
						// 	star[star[triangle[Math.floor(choosenQuestions[0]/6)][answers[registeredClick.button][i]]].adj[1]].id);
						options[i].text.setText(star[star[triangle[Math.floor(choosenQuestions[0]/6)][answers[registeredClick.button][i]]].adj[0]].id+
							star[star[triangle[Math.floor(choosenQuestions[0]/6)][answers[registeredClick.button][i]]].adj[1]].id);

                        if (star[triangle[Math.floor(choosenQuestions[0]/6)][answers[registeredClick.button][i]]].adj[1]%3 === 2) {
                            if (star[triangle[Math.floor(choosenQuestions[0]/6)][answers[registeredClick.button][i]]].adj[0]%3 === 0) {
                                // options[i].button.tint = "0xff0000";
                                options[i].text.addColor("#f00", 0);
                                // console.log("red "+i);
                            }
                            else if (star[triangle[Math.floor(choosenQuestions[0]/6)][answers[registeredClick.button][i]]].adj[0]%3 === 1) {
                                // options[i].button.tint = "0x0000ff";
                                options[i].text.addColor("#0aa", 0);
                                // console.log("blue "+i);
                            }

                        }

                        else {
                            options[i].text.addColor("#0f0", 0);
                        }
						// options[i].text.setText(star[triangle[Math.floor(choosenQuestions[0]/6)][answers[registeredClick.button][i]]].adj[0]+""+
						// 	star[triangle[Math.floor(choosenQuestions[0]/6)][answers[registeredClick.button][i]]].adj[1]);
					}
				}

			};

			// questOptions[registeredClick.button].button.tint = "0xff6600";

		}

		if (newQuestion){

			newQuestion = false;
			animation.option = true;
			question.enunciado.setText('');
			finalAnswer = ['N', 'N', 'N'];

			hold = game.width - question.expresion.x + 50*proportions.min;
			question.expresion.x += hold;
			question.enunciado.x += hold;

			for (var i=0; i<nOptions; i++){					//	elimina las opciones previas
				options[i].button.events.onInputDown.removeAll();
				options[i].button.visible = false;
				options[i].button.inputEnabled = true;
				options[i].checkImage.destroy();
				options[i].text.setText('');
				questOptions[i].text.setText(questOptText[i]);
				questOptions[i].button.x += hold;
				questOptions[i].text.x += hold;
				// questOptions[i].button.tint = "0xffffff";
			}

            // star.map(function(ra) {
			// 	ra.obj.tint = "0xffffff";
			// });
										//	SE PASO A LA SIGUIENTE ETAPA

			if (winLevel){						//	GANAR

				winLevel = false;
				// nextLvlButton = this.add.button(game.width*0.8, game.height*0.5,
				//  'button', this.nextLevel, this);
				// nextLvlButton.anchor.setTo(0.5, 0.5);
				// nextLvlButton.scale.setTo(0.5*proportions.min, 0.5*proportions.min);
				// nextLvlButton.text = this.add.text(nextLvlButton.x, nextLvlButton.y, '',
				// 	{ font: '20px Trebuchet MS', fill: '#fff', align: 'center' });
				// nextLvlButton.text.anchor.setTo(0.5, 0.5);
				// nextLvlButton.text.setText('Siguiente');
				// nextLvlButton.text.scale.setTo(proportions.min, proportions.min);
                this.resultState();
				// readyButton.destroy();

				for(var i = 0; i < nOptions ; i++){
					options[i].button.destroy();
					options[i].text.destroy();
					questOptions[i].button.destroy();
					questOptions[i].text.destroy();
				}

			} else {

				if(addStars){

					questTriangle++;
					addStars = false;
					for(var i = 1; i < triangle[questTriangle].length; i++){
						star[triangle[questTriangle][i]].obj.visible = true;
						star[triangle[questTriangle][i]].text.visible = true;
					}
					trianLines[questTriangle].visible = true;
					trianLines[questTriangle + finalStars/3].visible = true;

					//	NUEVAS PREGUNTAS DEBIDO A NUEVO TRIANGULO
					hold = choosenQuestions.concat(this.createOrderArray(6*questTriangle, 6*(questTriangle+1)));
					// console.log("choosenQuestion: "+hold);
					choosenQuestions = this.shuffle(hold);
					// choosenQuestions = this.shuffle(this.createOrderArray(6*questTriangle, 6*(questTriangle+1)));
					console.log("choosenQuestion: "+choosenQuestions);

				};

                // star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].obj.tint = "0xffff00";

				if((choosenQuestions[0]%6)<2){
					correctOption = 0;		//	seno
					question.enunciado.setText("Complete la ecuación correctamente\n usando "+
						star[star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].op[0]].id+
						star[star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].op[1]].id);

					question.expresion.setText("Seno");
                    let catetoColor = (star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].op[0]%3) ? '#0aa' : '#f00';
					question.enunciado.addColor(catetoColor, 42);
					// answers[3][1] = 0;

                    pistas.lista = [];

                    pistas.lista.push("Acuerdate que para el ángulo " + star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].id + ",\n" +
                    "el cateto opuesto es " + star[star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].op[0]].id+
                    star[star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].op[1]].id);

                    pistas.lista.push("Acuerdate que para el ángulo " + star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].id + ",\n" +
                    "la hipotenusa es " + star[triangle[Math.floor(choosenQuestions[0]/6)][0]].id+star[triangle[Math.floor(choosenQuestions[0]/6)][1]].id);
				}
				else if((choosenQuestions[0]%6)<4){
					correctOption = 1;		//	coseno
					question.enunciado.setText("Complete la ecuación correctamente\n usando "+
						star[star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].adj[0]].id+
						star[star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].adj[1]].id);
					question.expresion.setText("Coseno");
                    let catetoColor = (star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].adj[0]%3) ? '#0aa' : '#f00';
					question.enunciado.addColor(catetoColor, 42);

                    pistas.lista = [];

                    pistas.lista.push("Acuerdate que para el ángulo " + star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].id + ",\n" +
                    "el cateto adyacente es " + star[star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].adj[0]].id+
                    star[star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].adj[1]].id);

                    pistas.lista.push("Acuerdate que para el ángulo " + star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].id + ",\n" +
                    "la hipotenusa es " + star[triangle[Math.floor(choosenQuestions[0]/6)][0]].id+star[triangle[Math.floor(choosenQuestions[0]/6)][1]].id);
				}
				else{
					correctOption = 2;		//	tangente
					question.enunciado.setText("Complete la ecuación correctamente\n usando "+
						star[star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].op[0]].id+
						star[star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].op[1]].id);
					question.expresion.setText("Tangente");
                    let catetoColor = (star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].op[0]%3) ? '#0aa' : '#f00';
					question.enunciado.addColor(catetoColor, 42);

                    pistas.lista = [];

                    pistas.lista.push("Acuerdate que para el ángulo " + star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].id + ",\n" +
                    "el cateto adyacente es " + star[star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].adj[0]].id+
                    star[star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].adj[1]].id);

                    pistas.lista.push("Acuerdate que para el ángulo " + star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].id + ",\n" +
                    "el cateto opuesto es " + star[star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].op[0]].id+
                    star[star[triangle[Math.floor(choosenQuestions[0]/6)][choosenQuestions[0]%2]].op[1]].id);

				};

                pistas.lista.push(
                    "Acuerdate que las expresiones\n"+
                    "trigonométricas se encuentran en\n"+
                    "el menú de ayuda que se encuentra\n"+
                    " al darle al botón de pausa");
                pistas.lista.push(
                    "Acuerdate que la constelación\n"+
                    "esta pintada de cierta forma,\n"+
                    "el color de los lados del enunciado\n"+
                    "corresponden a los colores de los\n"+
                    "trazos entre las estrellas");

                pistas.lista = this.shuffle(pistas.lista);
                pistas.text.setText("");


				for (var i = 0; i < nOptions; i++){				//	ubica las respuestas aleatorio
					answers[i] = [];
					for (var j = 0; j < nOptions; j++){
						answers[i][j] = j;
					}
					answers[i] = this.shuffle(answers[i]);
				};

				//	LLAVE PARA SENO Y COSENO
				keyAnswers[0] = [];
				keyAnswers[0][0] = this.findArray(answers[0], choosenQuestions[0]%2);
				keyAnswers[0][1] = this.findArray(answers[1], choosenQuestions[0]%2);
				keyAnswers[0][2] = this.findArray(answers[2], 2);

				//	LLAVES PARA TANGENTE
				keyAnswers[1] = [];
				keyAnswers[1][0] = this.findArray(answers[0], 0);
				keyAnswers[1][1] = this.findArray(answers[1], 1);
				keyAnswers[1][2] = this.findArray(answers[2], 0);

				keyAnswers[2] = [];
				keyAnswers[2][0] = this.findArray(answers[0], 1);
				keyAnswers[2][1] = this.findArray(answers[1], 0);
				keyAnswers[2][2] = this.findArray(answers[2], 1);

			};
		};


		if(rotateStars.en){
			for(var i=0; i <rotateStars.num; i++){
				star[rotateStars.theseStars[i]].obj.angle += 1;
			};
		};

},

	//	genera una matriz de dimension numbersNeeded de aleatorios no repetidos contando el 0 hasta el topLimit
Formularium.Constelaciones2_tutorial.prototype.notRepeatRandom = function (topLimit, numbersNeeded)
{
	for (var i = 0, ar = []; i < topLimit; i++) {
	    ar[i] = i+1;
	}

	ar.sort(function () {
	    return Math.random() - 0.5;
	});
	var numbersNotRepeated = [];
	for (var i = 0; i<numbersNeeded; i++){
		numbersNotRepeated.push(ar[ar.length-1]);
		ar.pop();
	}
	return numbersNotRepeated;
},

Formularium.Constelaciones2_tutorial.prototype.toggleCoordenates = function (star)
{
	if(onCoord){
		for(var i = 0;i<iStars; i++){
			star[i].text.setText(star[i].id);
		};
		onCoord = false;
	}
	else{
		for(var i = 0; i<iStars; i++){
			star[i].text.setText(star[i].id+':( '+star[i].x+', '+star[i].y+')');
		};
		onCoord = true;
	};
},


Formularium.Constelaciones2_tutorial.prototype.clickedOption = function (boton)
{
	questOptions[registeredClick.button].text.setText(options[boton.number].text.text);
	finalAnswer[registeredClick.button] = boton.number;
    options.map(function(button){
        button.text.visible = false;
        button.button.visible = false;
    });

},

Formularium.Constelaciones2_tutorial.prototype.nextLevel = function ()
{
	//YO MANUEL COMENTE ESTAS LINEAS
	// game.level++;
	// this.state.start('Parte2');
	var estrellas = 3-blackStars;
	if(game.global.starsArray[game.global.level-1]<estrellas)
	{
		game.global.starsArray[game.global.level-1] = estrellas;
		firebase.database().ref("/users/" + this.game.usuario.value + "/starsArray").set(game.global.starsArray);
	}
	let change = false;
	// if we completed a level and next level is locked - and exists - then unlock it
	if(estrellas>0 && game.global.starsArray[game.global.level]==4 && game.global.level<game.global.starsArray.length)
	{
		game.global.starsArray[game.global.level] = 0;
		change = true;
	}
	if (estrellas>0 && game.global.starsArray[game.global.level+3]==4 && game.global.level<game.global.starsArray.length-4)
	{
		game.global.starsArray[game.global.level+3] = 0;
		change = true;
	}
	if (change == true)
	{
		change = false;
		firebase.database().ref("/users/" + this.game.usuario.value + "/starsArray").set(game.global.starsArray);
	}
	this.game.state.start("BootState", true, false, "assets/levels/level_select.json", "LevelSelect");
},

Formularium.Constelaciones2_tutorial.prototype.clickQuestOptions = function (boton)
{
	registeredClick.click = true;
	registeredClick.button = boton.number;
},

Formularium.Constelaciones2_tutorial.prototype.createOrderArray = function (init, finish)
{
	var array = [];
	for (var i = init; i < finish; i++){
		array[i - init] = i;
	}
	return array;
},

Formularium.Constelaciones2_tutorial.prototype.shuffle = function (array)
{

	var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
	while (0 !== currentIndex) {

	    // Pick a remaining element...
	    randomIndex = Math.floor(Math.random() * currentIndex);
	    currentIndex -= 1;

	    // And swap it with the current element.
	    temporaryValue = array[currentIndex];
	    array[currentIndex] = array[randomIndex];
	    array[randomIndex] = temporaryValue;
  	}

	return array;
},

Formularium.Constelaciones2_tutorial.prototype.findArray = function (array, element)
{
	for (i = 0; i < array.length; i++){
		if (array[i] == element){
			return i;
		}
	}
	return -1;
},

Formularium.Constelaciones2_tutorial.prototype.checkAnswer = function ()
{

	console.log("Respuestas: "+finalAnswer);
	if(this.findArray(finalAnswer, 'N') != -1){
		console.log("Faltan respuestas");
	}
	else{

		if(((correctOption < 2)&&(finalAnswer[0] == keyAnswers[0][0])&&(finalAnswer[1] == keyAnswers[0][1])&&(finalAnswer[2] == keyAnswers[0][2]))
			||((correctOption == 2)&&(finalAnswer[0] == keyAnswers[1][0])&&(finalAnswer[1] == keyAnswers[1][1])&&(finalAnswer[2] == keyAnswers[1][2]))
			||((correctOption == 2)&&(finalAnswer[0] == keyAnswers[2][0])&&(finalAnswer[1] == keyAnswers[2][1])&&(finalAnswer[2] == keyAnswers[2][2]))){

			newQuestion = true;
			if(stage == nStages){
				winLevel = true;					//	GANAR
			} else {
				if ((stage == iStars)&&(stage < nStages)){
					console.log("mas estrellas");
					addStars = true;
					iStars+=2;
				}

				stage++;
				choosenQuestions.shift();		//	elimina la primera pregunta ya hecha, sigue la secuencia
			}
            star[rotateStars.theseStars[rotateStars.num]].emitter.start(false, 200, 100);
			rotateStars.num++;
		}
		else {
			fails++;
			animation.fail = true;
            let hold = pistas.lista.shift();
            pistas.text.setText(hold);
            pistas.lista.push(hold);
		}
	}
	statusText.setText('Nivel: '+game.level+' Etapa: '+stage+'/'+ nStages + ' Fallos: '+fails);
},

Formularium.Constelaciones2_tutorial.prototype.changeState = function (buton)
{

	changeState = true;
	buton.inputEnabled = false;
	if (buton.use == "help"){
		console.log(buton.use);
		gameState.next = "HELP";
	}
	else if (buton.use == "exit"){
		console.log(buton.use);
		gameState.next = "GAME";
	}
},

Formularium.Constelaciones2_tutorial.prototype.resultState = function () {
    let resultUI = {
        bg: 0,
        star: []
    };

    blackStars = fails;
    if (fails >= 1) {
        blackStars = 1;
    }
    if (fails >= 3) {
        blackStars = 2;
    }
    if (fails >= 6) {
        blackStars = 3;
    }

    resultUI.bg = game.add.sprite(game.width * 0.5, game.height *0.5, 'button');
    resultUI.bg.scale.setTo(proportions.min, 2*proportions.min);
    resultUI.bg.anchor.setTo(0.5);

    nextLvlButton = this.add.button(game.width*0.5, game.height*0.6, 'next', this.nextLevel, this);
    nextLvlButton.anchor.setTo(0.5, 0.5);
    nextLvlButton.scale.setTo(0.5*proportions.min);

    for (let i = 0; i < 3; i++) {
        resultUI.star[i] = game.add.sprite(game.width * 0.1 * (4 + i), game.height * (0.5 - ((i%2) * 0.1)), 'starScore');
        resultUI.star[i].scale.setTo(proportions.min*0.5);
        resultUI.star[i].anchor.setTo(0.5);
        if (3 - blackStars <= i) {
            resultUI.star[i].tint = "0x666666";
        }
    }
},

Formularium.Constelaciones2_tutorial.prototype.nextTutorial = function () {
    if (nextButton.currentState < nextButton.totalState) {
        nextButton.currentState++;
        nextButton.text.setText(tutorial[nextButton.currentState]+"\n"+
            nextButton.currentState+" \\ "+nextButton.totalState);
        // if (nextButton.currentState == 1) {
        // 	nextButton.image.loadTexture('rectTri');
        // }
        // if (nextButton.currentState == 1) {
        // 	nextButton.image.loadTexture('relation');
        // 	nextButton.image.scale.setTo(proportions.min*0.6, proportions.min*0.6);
        // }
    } else {
        nextButton.text.visible = false;
        nextButton.button.visible = false;
        nextButton.image.visible = false;

        statusText.visible = true;
        question.expresion.visible = true;
        question.enunciado.visible = true;
        readyButton.button.visible = true;
        readyButton.text.visible = true;
        eq.visible = true;
        for (var i = 0, n = options.length; i < n; i++) {
            //options[i].button.visible = true;
            //options[i].text.visible = true;
            questOptions[i].button.visible = true;
            questOptions[i].text.visible = true;
            questOptions[i].button.tint = "0x446600";
        }
    }
}

Formularium.Constelaciones2_tutorial.prototype.homeScreen = function ()
{
	//YO MANUEL COMENTE ESTA LINEA
	//this.state.start('Menu');
	this.game.state.start("BootState", true, false, "assets/levels/level_select.json", "LevelSelect");
}
